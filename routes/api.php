<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PassportController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['middleware' => ['app_check']], function(){

Route::post('register',[PassportController::class,'register']);
Route::post('login',[PassportController::class,'login']);
Route::post('forgot-password',[PassportController::class,'forgot_password']);
Route::post('social-check',[PassportController::class,'social_check']);
Route::post('password/reset',[PassportController::class,'reset']);
Route::get('test-mail',[PassportController::class,'test_mail']);
Route::get('privacy-policy',[PassportController::class,'privacy_policy']);
Route::get('terms-and-conditions',[PassportController::class,'terms_and_conditions']);
Route::get('faqs',[PassportController::class,'faqs']);
Route::get('about-us',[PassportController::class,'about_us']);
Route::post('paytm-payment-checksum',[PassportController::class,'paytm_payment_checksum']);
Route::get('categories', [PassportController::class,'categories']);
Route::post('subcategories',[PassportController::class,'subcategories']);
Route::post('products',[PassportController::class,'products']);
Route::get('product/{id}',[PassportController::class,'productDetail']);
Route::get('banners',[PassportController::class,'banners']);
Route::get('states',[PassportController::class,'states']);
Route::get('cities',[PassportController::class,'cities']);
Route::get('dashboard',[PassportController::class,'dashboard']);
// Apis for music section
Route::get('music-categories',[PassportController::class,'music_categories']);
Route::post('audios',[PassportController::class,'audios']);
Route::post('videos',[PassportController::class,'videos']);
Route::post('lyrics',[PassportController::class,'lyrics']);
Route::get('music-dashboard',[PassportController::class,'music_dashboard']);
});

// Apis with authentication
Route::group(['middleware' => ['auth:api','user_accessible','app_check']], function(){
Route::post('details', 'PassportController@details');

Route::post('edit-profile',[PassportController::class,'edit_profile']);
Route::get('get-profile',[PassportController::class,'get_profile']);
Route::post('change-password',[PassportController::class,'change_password']);
Route::post('logout',[PassportController::class,'logout']);

Route::post('add-address',[PassportController::class,'add_address']);
Route::post('order-details-save',[PassportController::class,'order_details_save']);
Route::get('timeslots',[PassportController::class,'timeslots']);

Route::post('cod-order',[PassportController::class,'cod_order']);
Route::post('paytm-callback',[PassportController::class,'paytmCallback']);
Route::get('myorders',[PassportController::class,'myorders']);
Route::post('promocodes',[PassportController::class,'promocodes']);
Route::post('favourite',[PassportController::class,'favourite']);
Route::get('favourites',[PassportController::class,'favourites']);
Route::get('deliverycharges',[PassportController::class,'deliverycharges']);
Route::get('notifications',[PassportController::class,'notifications']);
Route::get('notification-read/{id}',[PassportController::class,'notification_read']);
Route::post('notification-delete',[PassportController::class,'notification_delete']);





});

