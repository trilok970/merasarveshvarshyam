<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
define('SEGMENT',request()->segment(1));

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('success','UserContoller@success_message');
Route::get('privacy-policy','App\Http\Controllers\HomeController@privacy_policy');
Route::get('terms-and-conditions','App\Http\Controllers\HomeController@terms_and_conditions');
Route::get('faqs','App\Http\Controllers\HomeController@faqs');
Route::get('about-us','App\Http\Controllers\HomeController@about_us');
Route::get('notification','App\Http\Controllers\Admin\UserFcmTokenController@sendNotification');
Route::get('show-order-invoice/{id}','App\Http\Controllers\Admin\OrderController@show_order_invoice');

Route::resource('narega','App\Http\Controllers\NaregaController');
Route::get('narega-worker-list','App\Http\Controllers\NaregaController@narega_worker_list');
Route::get('narega-export','App\Http\Controllers\NaregaController@narega_export');



Route::group(['prefix'=>'admin','namespace'=>'App\Http\Controllers\Admin'],function(){

	Route::get('/', 'UserContoller@login');
	Route::get('login', 'UserContoller@login');
	Route::post('login', [ 'as' => 'login', 'uses' => 'UserContoller@login']);
	Route::post('do-login','UserContoller@do_login');
	Route::get('logout','UserContoller@logout');
	Route::view('forgot_password', 'auth.reset_password')->name('password.reset');
	Route::post('reset-password','UserContoller@reset_password');
    Route::get('save-info','UserContoller@saveinfo');
    Route::get('save-info1','UserContoller@saveinfo1');

	Route::middleware(['admin'])->group(function () {

		Route::get('dashboard','DashboardController@index');
        Route::get('my-profile','DashboardController@myProfile')->name('my.profile');
        Route::get('edit-profile','DashboardController@editProfile')->name('edit.profile');
        Route::post('edit-profile','DashboardController@editProfilePost')->name('edit.profile.post');
		Route::get('change-password','DashboardController@changePassword')->name('change.password');
        Route::post('change-password','DashboardController@changePasswordPost')->name('change.password.post');
		Route::resource('user','UserContoller');
		Route::post('update-status','UserContoller@update_status');
		Route::get('ajax','UserContoller@ajax');
		Route::resource('cms','CmsController');
		Route::resource('content','ContentController');
		Route::resource('category','CategoryController');
		Route::resource('subcategory','SubCategoryController');
		Route::resource('product','ProductController');
		Route::resource('banner','BannerController');
		Route::resource('promocode','PromoCodeController');
		Route::post('banner-add/{id}','PlaylistController@banner_add_post');
		Route::get('profile','UserContoller@profile')->name('profile');
        Route::get('orders/{id?}','OrderController@orders')->name('orders.index');
        Route::resource('order','OrderController');
        Route::get('/update-order-status/{order_id}/{user_id}/{order_Status}','OrderController@update_order_status');
        Route::post('/update-order-status','OrderController@update_order_status1');
        Route::resource('deliverycharge','DeliveryChargeController');
		Route::resource('musiccategory','MusicCategoryController');
		Route::resource('audio','AudioController');
		Route::resource('video','VideoController');
		Route::resource('lyric','LyricController');
		Route::resource('subunit','SubunitController');
		Route::resource('color','ColorController');
		Route::resource('unit','UnitController');



	});


});
