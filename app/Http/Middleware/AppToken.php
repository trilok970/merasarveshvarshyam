<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AppToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->header('accessToken') != env('APP_TOKEN')) {
            $data['status']     = false;
            $data['message']	= "App token does not match";
            return response()->json($data);
		}
        return $next($request);

    }

}
