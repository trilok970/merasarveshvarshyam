<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class UserAccessible
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $user = Auth::user();
        // echo '<pre>';
        // print_r($user);
        // exit;
        $data['status'] = false;
        if($user->status==0)
        {
            $data['message'] = 'Your account deactivated by administrator.Please contact to administrator.';
            return response()->json($data, 200);
        }
        if($user->is_deleted==1)
        {
            $data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
            return response()->json($data, 200);
        }
        return $next($request);
    }

}
