<?php

namespace App\Http\Controllers;

use App\Models\Narega;
use Illuminate\Http\Request;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\NaregaExport;
class NaregaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "check";exit;
        return view('narega.index');

    }
    public function narega_worker_list()
    {
        // echo "check";exit;
        $datas = Narega::orderBy('id','desc')->get();
        return view('narega.narega-worker-list',compact('datas'));
    }
    public function narega_export()
    {
        return Excel::download(new NaregaExport,'narega.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('narega.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(),[
            'name' => 'required',
            'father_name' => 'required',
            'mobile_no' => 'required|unique:naregas,mobile_no',
            'dob' => 'required',
            'post' => 'required',
            'date_of_joining' => 'required',
            'block' => 'required',
            'district' => 'required',
            'state' => 'required',
            // 'donation' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }

            $category = new Narega;
            $category->name = ucfirst($request->name);
            $category->father_name = ucfirst($request->father_name);
            $category->email = $request->email;
            $category->mobile_no = $request->mobile_no;
            $category->dob = date("Y-m-d",strtotime($request->dob));
            $category->post = $request->post;
            $category->date_of_joining = date("Y-m-d",strtotime($request->date_of_joining));
            $category->gram_panchayat = $request->gram_panchayat;
            $category->block = $request->block;
            $category->district = $request->district;
            $category->state = $request->state;
            $category->donation = $request->donation ?? "";
            $category->message = $request->message;
            if($category->save())
            {
                return redirect('narega')->with('message','Thank you for contacting us.');
            }
            else
            {
                return back()->with('message','Category Not Added');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Narega  $narega
     * @return \Illuminate\Http\Response
     */
    public function show(Narega $narega)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Narega  $narega
     * @return \Illuminate\Http\Response
     */
    public function edit(Narega $narega)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Narega  $narega
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Narega $narega)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Narega  $narega
     * @return \Illuminate\Http\Response
     */
    public function destroy(Narega $narega)
    {
        //
    }
}
