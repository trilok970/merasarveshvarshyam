var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, {pingInterval: 5000, pingTimeout: 3000, cookie: false});
var request = require('request');
var mysql = require('mysql');

var basepath = "http://demo2server.com/chilling_app/";
//var basepath = "http://192.168.0.53/holler_back/";
var sockets = {};
var onlineUser = [];

http.listen(3031, function () {
    console.log('listening on *:3031');
});



// demo2server.com
var con = mysql.createConnection({
 host: "23.239.31.218",
 user: "newtest_user",
 password: "<nineDemoSQLnewtest@2020>",
 database: "chilling_app"
});



app.post('/video_call',function(req, res){
    console.log('video_call request ',req.body.data);
    var params = JSON.parse(req.body.data);
    console.log('video_call user_id',params.user_id);

    var split_members = params.other_user_id.split(',');
    split_members.forEach(function (user_id, index) {
        console.log('user_id video call',user_id);
        var sdata = sockets[user_id];
        if (sdata != undefined) {
            var newArray = new Array();
            for (var i in sdata) {
                var sid = Object.keys(sdata[i]);
                if (!newArray.includes(sid)) {
                    if (sockets[user_id][i][sid] != undefined) {
                        console.log('video_call_response',params);
                        sockets[user_id][i][sid].emit('video_call_response', params);
                    }
                }
                newArray.push(sid);
            }
        }
    });
    res.end();
});

app.post('/audio_call',function(req, res){
    console.log('audio_call request ',req.body.data);
    var params = JSON.parse(req.body.data);
    console.log('audio_call user_id',params.user_id);

    var split_members = params.other_user_id.split(',');
    split_members.forEach(function (user_id, index) {
        console.log('user_id audio call',user_id);
        var sdata = sockets[user_id];
        if (sdata != undefined) {
            var newArray = new Array();
            for (var i in sdata) {
                var sid = Object.keys(sdata[i]);
                if (!newArray.includes(sid)) {
                    if (sockets[user_id][i][sid] != undefined) {
                        console.log('audio_call_response',params);
                        sockets[user_id][i][sid].emit('audio_call_response', params);
                    }
                }
                newArray.push(sid);
            }
        }
    });
    res.end();
});

con.connect(function (err) {
    if (err)
        throw err;
    console.log("Connected Successfully!");
});

io.on('connection', function (socket) {


    socket.on("audio_duration",function(msg){
        var formdata = {
           user_id : msg.user_id,
           audio_id : msg.audio_id,
           audio_current_time : msg.audio_current_time,
           playlist_id : msg.playlist_id
        };
        console.log("formdata == "+JSON.stringify(formdata));
        saveAudioDuration(formdata,socket);

    });

});







function saveAudioDuration(formdata,socket)
{
    var user_id = formdata.user_id;
    var audio_id = formdata.audio_id;
    var audio_current_time = formdata.audio_current_time;
    var playlist_id = formdata.playlist_id;
    if(playlist_id == '' || playlist_id == null)
        playlist_id = 0;

    var select_sql = "SELECT count(id) as total FROM `audio_durations` WHERE user_id= "+user_id+" and audio_id= "+audio_id+"";
    con.query(select_sql,function(err,result){
        if(err)
        throw err;
        console.log("result == "+result[0].total);
        if(result[0].total == 1)
        {
            var insert_sql = "UPDATE `audio_durations` SET `audio_current_time` = '"+audio_current_time+"' where user_id = "+user_id+" and audio_id = "+audio_id+" ";
            con.query(insert_sql,function(err,result){
                if(err)
                {
                    var data = {"status":false,"data":""};
                    socket.emit('audio_duration', data);
                    throw err;
                }
                else
                {
                    var data = {"status":true,"data":formdata};
                    socket.emit('audio_duration', data);
                    console.log("audio duration updated successfully");
                }

            });
        }
        else
        {
            var insert_sql = "insert into audio_durations (user_id,audio_id,audio_current_time) values("+user_id+","+audio_id+",'"+audio_current_time+"')";
            con.query(insert_sql,function(err,result){
                if(err)
                {
                    var data = {"status":false,"data":""};
                    socket.emit('audio_duration', data);
                    throw err;
                }
                else
                {
                    var data = {"status":true,"data":formdata};
                    socket.emit('audio_duration', data);
                    console.log("audio duration inserted successfully");
                }

            });
        }
        savePlaylistCurrentAudio(user_id,audio_id,playlist_id);

    });


}
function savePlaylistCurrentAudio(user_id,audio_id,playlist_id)
{
    var select_sql = "SELECT count(id) as total FROM `playlist_current_audio` WHERE user_id= "+user_id+" and playlist_id= "+playlist_id+"";
    con.query(select_sql,function(err,result){
        if(err)
            throw err;
        if(result[0].total == 1)
        {
            var insert_sql = "UPDATE `playlist_current_audio` SET `audio_id` = '"+audio_id+"' where user_id = "+user_id+" and playlist_id = "+playlist_id+" ";
            con.query(insert_sql,function(err,result){
                if(err)
                    throw err;
                else
                {
                    console.log("playlist current audio updated successfully");
                }

            });
        }
        else
        {
            var insert_sql = "insert into playlist_current_audio (user_id,audio_id,playlist_id) values("+user_id+","+audio_id+",'"+playlist_id+"')";
            con.query(insert_sql,function(err,result){
                if(err)
                    throw err;
                else
                    console.log("playlist current audio inserted successfully");

            });
        }
    })
    
}








