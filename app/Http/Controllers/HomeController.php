<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cms;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('privacy_policy','terms_and_conditions','faqs','about_us');
    }
    public function privacy_policy()
    {
        $page_title = "Privacy Policy";
        $cms = Cms::where('slug','privacy-policy')->first();
        return view('privacy-policy',compact('page_title','cms'));
    }
    public function terms_and_conditions()
    {
        $page_title = "Terms And Conditions";
        $cms = Cms::where('slug','terms-and-conditions')->first();
        return view('terms-and-conditions',compact('page_title','cms'));
    }
    public function faqs()
    {
        $page_title = "Faqs";
        $cms = Cms::where('slug','faqs')->first();
        return view('faqs',compact('page_title','cms'));
    }
    public function about_us()
    {
        $page_title = "About Us";
        $cms = Cms::where('slug','about-us')->first();
        return view('faqs',compact('page_title','cms'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
