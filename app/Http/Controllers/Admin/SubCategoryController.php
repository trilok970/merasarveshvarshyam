<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Subcategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use ImageResize;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo "string";dd();
        $datas = Subcategory::where(['is_deleted'=>0])->with('category')->orderBy('id','desc')->get();
        // echo "<pre>";
        // print_r($datas[0]->get_category);
        // dd();

        return view(SEGMENT.'/subcategory.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view(SEGMENT.'/subcategory.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'category_id' => 'required',
        'name' => 'required',
        'image' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "uploads/images/".time().".$extension";
                $ff = $request->image->move(public_path('uploads/images'),$fileName);


                
                
                $thumbnail = time().".$extension";;
                $destinationPath = public_path('/uploads/thumbnail');
                $img = ImageResize::make($ff->getRealPath());
                $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
                })->save($destinationPath.'/'.$thumbnail);

                $thumbnail = "uploads/thumbnail/".$thumbnail;
                       
            }
            else
            {
                $fileName  ='default.jpg';
                $thumbnail  ='default.jpg';
            }
        $subcategory = new Subcategory;
        $subcategory->name = ucfirst($request->name);
        $subcategory->category_id = $request->category_id;
        $subcategory->image = $fileName;
        $subcategory->thumbnail = $thumbnail;
        if($subcategory->save())
        {
            return redirect(SEGMENT.'/subcategory')->with('message','Subcategory added successfully');
        }
        else
        {
            return back()->with('message','Sub category not added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategory $subcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory)
    {
        //
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view(SEGMENT.'/subcategory.edit',compact('subcategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory)
    {
        //
        $validator = validator::make($request->all(),[
        'category_id' => 'required',
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "uploads/images/".time().".$extension";
                $ff = $request->image->move(public_path('uploads/images'),$fileName);


                
                
                $thumbnail = time().".$extension";;
                $destinationPath = public_path('/uploads/thumbnail');
                $img = ImageResize::make($ff->getRealPath());
                $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
                })->save($destinationPath.'/'.$thumbnail);

                $thumbnail = "uploads/thumbnail/".$thumbnail;
                       
            }
            else
            {
                $fileName  = $subcategory->image;
                $thumbnail  = $subcategory->thumbnail;
            }
        $subcategory->name = ucfirst($request->name);
        $subcategory->category_id = $request->category_id;
        $subcategory->image = $fileName;
        $subcategory->thumbnail = $thumbnail;

        if($subcategory->save())
        {
            return redirect(SEGMENT.'/subcategory')->with('message','Subcategory updated successfully');
        }
        else
        {
            return back()->with('message','Subcategory not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        //
        $subcategory->is_deleted = 1;
        if($subcategory->save())
        {
            return redirect(SEGMENT.'/subcategory')->with('message','Subcategory deleted successfully');
        }
        else
        {
            return back()->with('message','Subcategory not deleted');
        }
    }
}
