<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\PromoCode;
use App\Models\Banner;
use App\Models\Color;
use App\Models\Product;
use App\Models\Subunit;
use App\Models\Unit;
use Validator;
use DB;
use Session;
use Hash;
use Artisan;
use Illuminate\Support\Facades\Password;
class UserContoller extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no') ?? 50;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset_password(Request $request)
    {

        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed|min:8|max:20'
        ]);

// echo "<pre>";
//         dd(request());
//         exit;
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();

            User::where(['email'=>$user->email])->update(['password'=>$user->password]);
        });
        $status=1;
        if ($reset_password_status == Password::INVALID_TOKEN) {
            $status=0;
            // return response()->json(["status"=>false,"message" => "Invalid token provided"], 400);
            $message = "Invalid token provided.";
            return redirect('success')->with('error_message',$message);
        }

        // return response()->json(["status"=>true,"message" => "Password has been successfully changed"]);
        $message = "Password has been changed successfully.";
        return redirect('success')->with('message',$message);
    }
    public function success_message()
    {
        return view('auth.success');
    }
    public function login()
    {
        return view('auth.login');
    }
    public function do_login(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);exit;
        $validator = Validator::make($request->all(),[
            'email'=>'required',
            'password'=>'required',
        ]);
        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }
        $count = Admin::where(['email'=>$request->email])->count();

        if($count > 0)
        {
            if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password,'id'=>1]))
            {
                return redirect(SEGMENT.'/dashboard');
            }
            else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
        }
        else
            {
                return back()->with('error_message','Email or password are incorrect');
            }
    }
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect(SEGMENT);
    }
    public function index(Request $request)
    {
        //
        if($request->query())
        {
            $search = trim($request->search);
            $users = User::where(['is_deleted'=>0])
            ->where(function($query) use ($search){
                $query->orWhere('fullname','like','%'.$search.'%');
                $query->orWhere('phone_number','like','%'.$search.'%');
                $query->orWhere('email','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderByDesc('id')->paginate($this->paginate_no);
        }
        else
        {
            $users = User::where(['is_deleted'=>0])->where('id','!=',1)->orderByDesc('id')->paginate($this->paginate_no);
            $search = '';
        }

        return view(SEGMENT.'/user.index',compact('users','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
       $user->is_deleted = 1;
       if($user->save())
       {
        // $token =  DB::table('oauth_access_tokens')
        //     ->where('user_id', $user->id)
        //     ->update(['revoked'=>1]);
        return back()->with('message','User Deleted Successfully');
       }
       else
        return back()->with('message','Something Went Wrong');

    }

    public function update_status(Request $request)
    {

        if($request->type=="channel")
        {
        $banner = Channel::where('id',$request->id)->first();
        $banner->status = $request->status;
        // $this->send_update_notification("banner");
        }
        else if($request->type=="category")
        {
            $banner = Category::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("category");
        }
        else if($request->type=="subcategory")
        {
            $banner = Subcategory::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("subcategory");
        }
        else if($request->type=="product")
        {
            $banner = Product::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("innercategory");
        }
        else if($request->type=="banner")
        {
            $banner = Banner::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="promocode")
        {
            $banner = PromoCode::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="subunit")
        {
            $banner = Subunit::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="color")
        {
            $banner = Color::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        else if($request->type=="unit")
        {
            $banner = Unit::where('id',$request->id)->first();
            $banner->status = $request->status;
            // $this->send_update_notification("product");
        }
        elseif($request->type=="users")
        {
        $banner = User::where('id',$request->id)->first();
        $banner->status = $request->status;

        if($request->status==0)
        {
            // $token =  DB::table('oauth_access_tokens')
            // ->where('user_id', $banner->id)
            // ->update(['revoked'=>1]);
            // print_r($token);
        }


        }
        if($banner->save())
        {

             // session()->put('success','Status change successfully');
            $data['status'] = 1;
            return response()->json($data);
        }
        else{
            session()->put('warning','Status not change successfully');
            $data['status'] = 0;
            return response()->json($data);
        }
    }


public function ajax(Request $request)
     {

        $data = '';
     if($request->type=='innercategory')
     {

        $subcategories = Innercategory::select('id','name')->where(['sub_category_id'=>$request->subcategory_id,'category_master_id'=>$request->category_master_id,'is_deleted'=>0,'status'=>1])->get();
        $data .= '<option value="0">-Select-</option>';
     }

     else if($request->type=='subcategory')
     {
        $subcategories = Subcategory::select('id','name')->where(['category_id'=>$request->category_id,'is_deleted'=>0,'status'=>1])->get();
        $data .= '<option value="0">-Select-</option>';
     }
     else if($request->type=='subunit')
     {
        $subunits = Subunit::select('id','name')->where(['unit_id'=>$request->unit_id,'is_deleted'=>0,'status'=>1])->get();
        $colors = Color::select('id','code')->where(['is_deleted'=>0,'status'=>1])->get();
        $data .= ' <div class="form-group row">
        <label class="col-md-2 m-t-15">Unit</label>
        <label class="col-md-2 m-t-15">Price</label>
        <label class="col-md-2 m-t-15">Mrp</label>
        <label class="col-md-2 m-t-15">Qty</label>
        <label class="col-md-1 m-t-15">Min Qty</label>
        <label class="col-md-1 m-t-15">Max qty</label>
        <label class="col-md-2 m-t-15">Colors</label>
    </div>';
    if($subunits) {
        
            $data .= '<div class="form-group row subunit_date_div">
            <label class="col-md-2 m-t-15"><select class="subunit_id form-control" name="subunit_id[]">';
            $data .= '<option value="">Select Subunit</option>';

            foreach($subunits as $subunit)
            {
                $data .= '<option value="'.$subunit->id.'">'.$subunit->name.'</option>';
            }
            $data .= '</select></label>
            <label class="col-md-2 m-t-15"><input type="number" min="0" class="form-control price" id="price" name="price[]" placeholder="Price" value="">
            </label>
            <label class="col-md-2 m-t-15"><input type="number" class="form-control mrp" id="mrp" name="mrp[]" placeholder="Mrp" step="1" min="0" pattern="^\d*(\.\d{0,2})?$" value="">
            </label>
            <label class="col-md-2 m-t-15"><input type="number" min="0" class="form-control qty" id="qty" name="qty[]" placeholder="Qty" value="">
            </label>
            <label class="col-md-1 m-t-15"><input type="number" min="0" class="form-control min_qty" id="min_qty" name="min_qty[]" placeholder=" Min Qty" value="">
            </label>
            <label class="col-md-1 m-t-15"><input type="number" min="0" class="form-control max_qty" id="max_qty" name="max_qty[]" placeholder="Max Qty" value="">
            </label>
            <label class="col-md-2 m-t-15"><select class="color_id form-control" name="color_id[]">';
            $data .= '<option value="">Select color</option>';

            foreach($colors as $color)
            {
                $data .= '<option value="'.$color->id.'"><div class="color_div" style="background-color:'.$color->code.';"> </div>'.$color->code.'</option>';
            }
            $data .= '</select></label>
        </div>';
    
          
    }
      return ($data);
      exit;
     }

       // print_r($subcategories);

      // exit;
      foreach($subcategories as $subcategorie)
      {
        $data .= '<option name="'.ucfirst($subcategorie->name).'" value="'.$subcategorie->id.'">'.$subcategorie->name.'</option>';

      }
     return ($data);



    }



    public function profile()
    {
        return view('admin.commons.coming-soon');
    }
     public function animatedBanner()
    {
        return view('admin.commons.animated-banner');
    }
     public function ambianceAudio()
    {
        return view('admin.commons.ambiance-audio');
    }
    public function check_ticketNo($ticketNo)
    {
        return DB::table('saveinfos')->where('ticket_no',$ticketNo)->count();
    }
    public function saveinfo(Request $request)
    {
        // echo app_path() . '\Functions\SimpleHtmlDom.php';exit;
        // require(app_path() . '\Functions\SimpleHtmlDom.php');

        $ticketNo = 370000;
        $start = 0;
        $end = 100;
        if(isset($request->start)) {
            $start = $request->start;
        }
        if(isset($request->end)) {
            $end = $request->end;
        }

        for($i = $start; $i <= $end; $i++) {
            $ticketNo = $ticketNo+$i;
        $html = file_get_html('https://shrishyamdarshan.in/darshan-booking/ticket/search?ticketNo='.$ticketNo.'&submit_search=Search');
        $title = $html->find('table', 0);
        if($title) {
            $name = $html->find('table', 0)->find('tr',5)->find('td',1);
        $contact_no = $html->find('table', 0)->find('tr',6)->find('td',1);
        $email = $html->find('table', 0)->find('tr',7)->find('td',1);

        $name = trim(preg_replace('/<[^>]*>/','',$name));
        $contact_no = trim(preg_replace('/<[^>]*>/','',$contact_no));
        $email = trim(preg_replace('/<[^>]*>/','',$email));
        $data = array("ticket_no"=>$ticketNo,"name"=>$name,
        "contact_no"=>$contact_no,"email"=>$email);
        // $dd[] = $name;
        // echo $title."<br>\n";
        // echo $name."<br>\n";
        // echo $contact_no."<br>\n";
        // echo $email."<br>\n";
        if(($this->check_ticketNo($ticketNo)) == 0) {
        DB::table('saveinfos')->insert($data);

        }
        }

        }
        echo "Data Added Successfully";
        // echo "<pre>";
        // var_dump($dd);
    }
    public function saveinfo1(Request $request)
    {

        $date = date("m/d/Y");
        $game_type = "PICK3";
        if(isset($request->date)) {
            $date = date("m/d/Y",strtotime($request->date));
        }
        if(isset($request->game_type)) {
            $game_type = strtoupper($request->game_type);
        }
        $gameNameIn = $game_type;
        $d = 12;
        $m = date("m",strtotime($date));
        $y = date("Y",strtotime($date));
    // echo 'http://fllottery.com/site/winningNumberSearch?searchTypeIn=date&gameNameIn='.$gameNameIn.'&singleDateIn='.$m.'%2F'.$d.'%2F'.$y.'';exit;
    // exit;
        $html = file_get_html('http://fllottery.com/site/winningNumberSearch?searchTypeIn=date&gameNameIn='.$gameNameIn.'&singleDateIn='.$m.'%2F'.$d.'%2F'.$y.'');


        $title = $html->find('table', 0);

        if($title) {
            $mid_no = $html->find('table', 0)->find('tr',0)->find('td',0);
		$mid_winner = $html->find('table', 0)->find('tr',2)->find('td',0);
		$mid_total_payout = $html->find('table', 0)->find('tr',2)->find('td',1);



		$mid_no = trim(preg_replace('/<[^>]*>/','',$mid_no));
		$mid_no = preg_replace('/[^0-9]/','',$mid_no);



		$mid_fireball = substr($mid_no,"-1");

		$mid_winner = trim(preg_replace('/<[^>]*>/','',$mid_winner));
		$mid_total_payout = trim(preg_replace('/<[^>]*>/','',$mid_total_payout));




		if($game_type == 'PICK3') {
			$eve_no = $html->find('table', 0)->find('tr',0)->find('td',1);
			$eve_no = trim(preg_replace('/<[^>]*>/','',$eve_no));
			$eve_no = preg_replace('/[^0-9]/','',$eve_no);
			$eve_winning_no = substr($eve_no,"0","3");
			$eve_fireball = substr($eve_no,"-1");
			$mid_winning_no = substr($mid_no,"0","3");
			$eve_winning_no = substr($eve_no,"0","3");
			$eve_winner = $html->find('table', 0)->find('tr',2)->find('td',2);
			$eve_total_payout = $html->find('table', 0)->find('tr',2)->find('td',3);

			$eve_winner = trim(preg_replace('/<[^>]*>/','',$eve_winner));
			$eve_total_payout = trim(preg_replace('/<[^>]*>/','',$eve_total_payout));
		}
		else {
			$eve_no = $html->find('table', 0)->find('tr',3)->find('td',0);
			$eve_no = trim(preg_replace('/<[^>]*>/','',$eve_no));
			$eve_no = preg_replace('/[^0-9]/','',$eve_no);
			$eve_winning_no = substr($eve_no,"0","3");
			$eve_fireball = substr($eve_no,"-1");

			$mid_winning_no = substr($mid_no,"0","4");
			$eve_winning_no = substr($eve_no,"0","4");

			$eve_winner = $html->find('table', 0)->find('tr',5)->find('td',0);
			$eve_total_payout = $html->find('table', 0)->find('tr',5)->find('td',1);

			$eve_winner = trim(preg_replace('/<[^>]*>/','',$eve_winner));
			$eve_total_payout = trim(preg_replace('/<[^>]*>/','',$eve_total_payout));
		}


        $data = array(
            "date"=>$date,
            "midday_winning_no"=>$mid_winning_no,
            "midday_fireball"=>$mid_fireball,
            "midday_winner"=>$mid_winner,
            "midday_total_payout"=>$mid_total_payout,
            "evening_winning_no"=>$eve_winning_no,
            "evening_fireball"=>$eve_fireball,
            "evening_winner"=>$eve_winner,
            "evening_total_payout"=>$eve_total_payout,
            );
        // $dd[] = $name;
        // echo $title."<br>\n";
        // echo $name."<br>\n";
        // echo $contact_no."<br>\n";
        // echo $email."<br>\n";


        return json_encode(["status"=>true,"message"=>"Lottery data found.","data"=>$data]);
                // echo "<pre>";
                // var_dump($dd);
        }
        else {
            return json_encode(["status"=>false,"message"=>"Lottery data not found.","data"=>[]]);
        }
    }





    ////////////////////////////////////////////////////////////////////////////////////////////////
}
