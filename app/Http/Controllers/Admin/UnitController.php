<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Unit;
use Illuminate\Http\Request;
use Validator;
class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Unit::where(['is_deleted'=>0])->get();
        return view('admin.unit.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
            'name' => 'required',
            ]);
            if($validator->fails())
            {
                return response()->json(['status' => 0, 'errors' => $validator->errors()]);
            }
            
            $unit = new Unit();
            $unit->name = $request->name;
            if($unit->save())
            {
                return response()->json(['status' => 1, 'msg' => 'Unit added successfully']);
            }
            else
            {
                return response()->json(['status' => -1, 'msg' => 'Unit not added']);
            }
        }
        catch (\Throwable $e) {
            return response()->json(['status' => -1, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        return view('admin.unit.edit',compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        try {
            $validator = Validator::make($request->all(),[
            'name' => 'required',
            ]);
            if($validator->fails())
            {
                return response()->json(['status' => 0, 'errors' => $validator->errors()]);
            }
            
            $unit->name = $request->name;
            if($unit->save())
            {
                return response()->json(['status' => 1, 'msg' => 'Unit added successfully']);
            }
            else
            {
                return response()->json(['status' => -1, 'msg' => 'Unit not added']);
            }
        }
        catch (\Throwable $e) {
            return response()->json(['status' => -1, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        $unit->is_deleted = 1;
        if($unit->save()) {
            return redirect('admin/unit')->with('Unit deleted successfully.');
        }
        else {
            return back()->with('Unit not deleted.');
        }
    }
}
