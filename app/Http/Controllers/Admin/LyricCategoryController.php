<?php

namespace App\Http\Controllers;

use App\Models\LyricCategory;
use Illuminate\Http\Request;

class LyricCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LyricCategory  $lyricCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LyricCategory $lyricCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LyricCategory  $lyricCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(LyricCategory $lyricCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LyricCategory  $lyricCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LyricCategory $lyricCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LyricCategory  $lyricCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LyricCategory $lyricCategory)
    {
        //
    }
}
