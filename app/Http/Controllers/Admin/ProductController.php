<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Category;
use App\Models\Color;
use App\Models\Subcategory;
use App\Models\ProductImage;
use App\Models\ProductSubunit;
use App\Models\Subunit;
use Illuminate\Http\Request;
use DB;
use ImageResize;
use Validator;
class ProductController extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $products = Product::where(['is_deleted'=>0])->orderByDesc('id')->get();
         $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        if($request->category_id){
            $category_id = $request->category_id;
            $subcategories = Subcategory::where(['is_deleted'=>0,'status'=>1,'category_id'=>$category_id])->orderByDesc('id')->get();
        }
        else {
            $category_id = 0;
            $subcategories = '';
        }
        if($request->subcategory_id){
            $subcategory_id = $request->subcategory_id;
        }
        else {
            $subcategory_id = 0;
        }
        if($request->search){
            $search = $request->search;
        }
        else {
            $search = '';
        }
         if($request->query())
        {
            $search = trim($search);
            $search = str_replace("’","'",$search);

            $products = Product::select(DB::raw("DISTINCT(products.id)"),'products.*')->where(['products.is_deleted'=>0,'products.status'=>1])
            ->leftjoin("categories as cat",function($join){
                $join->on("cat.id",'=','products.category_id');
            })
            ->when($search,function($q) use ($search){
                $q->where(function($query) use ($search){
                    $query->orWhere('products.name','like','%'.$search.'%');
                    $query->orWhere('cat.name','like','%'.$search.'%');
                    $query->orWhere('products.updated_at','like','%'.date('Y-m-d',strtotime($search)).'%');
                });
            })
            ->when($category_id,function($q) use ($category_id){
                $q->where('products.category_id',$category_id);
            })
            ->when($subcategory_id,function($q) use ($subcategory_id){
                $q->where('products.subcategory_id',$subcategory_id);
            })
            // ->orderBy('products.id','desc')->paginate($this->paginate_no);
            ->orderBy('products.id','desc')->get();
        }
        else
        {
            $products = Product::where(['is_deleted'=>0])->orderBy('id','desc')
            // ->paginate($this->paginate_no);
            ->get();
            $search = '';
        }

        return view(SEGMENT.'.product.index',compact('products','search','categories','category_id','subcategory_id','subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        $units = DB::table('units')->orderByDesc('id')->get();

        return view(SEGMENT.'.product.create',compact('categories','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                // 'hindi_name' => 'required',
                'category_id' => 'required',
                'subcategory_id' => 'required',
                'qty.*' => 'required',
                'min_qty.*' => 'required',
                'max_qty.*' => 'required',
                'mrp.*' => 'required',
                'price.*' => 'required',
                'subunit_id.*' => 'required',
                // 'description' => 'required',
                'image' => 'required',
                'thumbnail' => 'required',
                'unit' => 'required',
            ]);
            if($validator->fails())
            {
                // return back()
                // ->withInput()
                // ->withErrors($validator);
                return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    
            }
            $images=array();
    
        if($request->hasFile('image'))
          {
            if($files=$request->file('image')){
                    foreach($files as $file){
                        $name = "uploads/images/".time().$file->getClientOriginalName();
                        $ff = $file->move(public_path('/uploads/images'),$name);
    
                        $thumbnail = time().$file->getClientOriginalName();
                        $destinationPath = public_path('/uploads/images');
                        $img = ImageResize::make($ff->getRealPath());
                        // $img->resize(1080, 1080, function ($constraint) {
                        // $constraint->aspectRatio();
                        // })->save($destinationPath.'/'.$thumbnail);
                        $img->orientate()
                        ->resize(1080, 1080, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'/'.$thumbnail);
    
                        $images[]=$name;
                    }
                }
    
                  $fileName = implode("|",$images);
    
            // $extension = $request->image->extension();
            // $fileName  = "uploads/images/".time().".$extension";
            // $ff = $request->image->move(public_path('uploads/images'),$fileName);
    
            // $thumbnail = time().".$extension";;
            //         $destinationPath = public_path('/uploads/images');
            //         $img = ImageResize::make($ff->getRealPath());
            //         $img->resize(500, 500, function ($constraint) {
            //         $constraint->aspectRatio();
            //         })->save($destinationPath.'/'.$thumbnail);
          }
          else
          {
                $fileName = "default.jpg";
          }
    
          if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
          {
            $extension1 = $request->thumbnail->extension();
            $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
            $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);
    
           $thumbnail1 = time().".$extension1";;
                    $destinationPath1 = public_path('/uploads/thumbnail');
                    $img1 = ImageResize::make($ff1->getRealPath());
                    // $img1->resize(200, 200, function ($constraint) {
                    // $constraint->aspectRatio();
                    // })->save($destinationPath1.'/'.$thumbnail1);
    
                    $img1->orientate()
                    ->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath1.'/'.$thumbnail1);
          }
          else
          {
                $fileNameThumb = "default.jpg";
          }
    
           // echo $fileName;exit;
            $product = new Product();
            $product->name = ucwords($request->name);
            $product->hindi_name = $request->hindi_name ?? "";
            $product->category_id = $request->category_id ?? 0;
            $product->subcategory_id = $request->subcategory_id ?? 0;
            $product->code = $request->code ?? 0;
            $product->description = $request->description ?? "";
            // $product->qty = $request->qty ?? 0;
            // $product->min_qty = $request->min_qty ?? 0;
            // $product->max_qty = $request->max_qty ?? 0;
            // $product->mrp = $request->mrp ? $request->mrp:0;
            // $product->price = $request->price ? $request->price:0;
            $product->image = $fileName;
            $product->thumbnail = $fileNameThumb;
            $product->weight = $request->weight ?? 0;
            $product->unit = $request->unit ?? 0;
            if($product->save())
            {
    
                if(count($images) > 0)
                {
                    foreach ($images as $key => $value) {
                        $productimages = new ProductImage;
                        $productimages->product_id = $product->id;
                        $productimages->image = $value;
                        $productimages->save();
                    }
    
                }
                if(count($request->price) > 0)
                {
                    $i = 0;
                    foreach ($request->price as $key => $p_value) {
                        $productsubunit = new ProductSubunit();
                        // $productsubunit = [];
                        $productsubunit['product_id'] = $product->id ?? 0;
                        $productsubunit['subunit_id'] = $request->subunit_id[$i] ?? 0;
                        $productsubunit['price'] = $request->price[$i] ?? 0;
                        $productsubunit['qty'] = $request->qty[$i] ?? 0;
                        $productsubunit['mrp'] = $request->mrp[$i] ?? 0;
                        $productsubunit['min_qty'] = $request->min_qty[$i] ?? 0;
                        $productsubunit['max_qty'] = $request->max_qty[$i] ?? 0;
                        $productsubunit['color_id'] = $request->color_id[$i] ?? 0;
                        $productsubunit->save();
                        // $product->subunits()->attach($product->id,$productsubunit);
                        $i++;
                    }
                }
            // $type = "Category Added Suceesfully";
            // $msg = "your";
            // $this->get_notification($type, $msg);
            
                return response()->json(['status' => 1, 'msg' => 'Product added successfully']);
           
                // return redirect(SEGMENT.'/product')->with('message','Product added successfully');
            }
        }
        catch (\Throwable $e) {
            return response()->json(['status' => -1, 'msg' => $e->getMessage()]);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::where('id',$product->id)->with('ProductSubunit.product','ProductSubunit.subunit')->first();
        //  echo "<pre>";
        // print_r($product);
        // exit;
        $categories = Category::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->get();
        $subcategories = Subcategory::where(['is_deleted'=>0,'category_id'=>$product->category_id,'status'=>1])->orderByDesc('id')->get();
        $subunits = Subunit::where(['is_deleted'=>0,'unit_id'=>$product->unit,'status'=>1])->orderByDesc('id')->get();
        $colors = Color::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->get();
        //  echo "<pre>";
        // print_r($subunits);
        // exit;
        $units = DB::table('units')->orderByDesc('id')->get();

        return view(SEGMENT.'.product.edit',compact('categories','units','product','subcategories','subunits','colors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        try{

        
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            // 'hindi_name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'qty.*' => 'required',
            'min_qty.*' => 'required',
            'max_qty.*' => 'required',
            'mrp.*' => 'required',
            'price.*' => 'required',
            // 'description' => 'required',
            'unit' => 'required',
        ]);
        if($validator->fails())
        {
            // return back()
            // ->withInput()
            // ->withErrors($validator);
            return response()->json(['status' => 0, 'errors' => $validator->errors()]);

        }
        $images=array();

    if($request->hasFile('image'))
      {
        if($files=$request->file('image')){
                foreach($files as $file){
                    $name = "uploads/images/".time().$file->getClientOriginalName();
                    $ff = $file->move(public_path('/uploads/images'),$name);

                    $thumbnail = time().$file->getClientOriginalName();
                    $destinationPath = public_path('/uploads/images');
                    $img = ImageResize::make($ff->getRealPath());

                    // $img->resize(1080, 1080, function ($constraint) {
                    // $constraint->aspectRatio();
                    // })->save($destinationPath.'/'.$thumbnail);

                    $img->orientate()
                    ->resize(1080, 1080, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);

                    $images[]=$name;
                }
            }

              $fileName = implode("|",$images);

        // $extension = $request->image->extension();
        // $fileName  = "uploads/images/".time().".$extension";
        // $ff = $request->image->move(public_path('uploads/images'),$fileName);

        // $thumbnail = time().".$extension";;
        //         $destinationPath = public_path('/uploads/images');
        //         $img = ImageResize::make($ff->getRealPath());
        //         $img->resize(500, 500, function ($constraint) {
        //         $constraint->aspectRatio();
        //         })->save($destinationPath.'/'.$thumbnail);
      }
      else
      {
            $fileName = $product->image;
      }

      if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
      {
        $extension1 = $request->thumbnail->extension();
        $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
        $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);

       $thumbnail1 = time().".$extension1";;
                $destinationPath1 = public_path('/uploads/thumbnail');
                $img1 = ImageResize::make($ff1->getRealPath());
                // $img1->resize(200, 200, function ($constraint) {
                // $constraint->aspectRatio();
                // })->save($destinationPath1.'/'.$thumbnail1);
                $img1->orientate()
                ->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath1.'/'.$thumbnail1);
      }
      else
      {
            $fileNameThumb = $product->thumbnail;
      }

       // echo $fileName;exit;

        $product->name = ucwords($request->name);
        $product->hindi_name = $request->hindi_name ?? "";
        $product->category_id = $request->category_id ?? 0;
        $product->subcategory_id = $request->subcategory_id ?? 0;
        $product->code = $request->code ?? 0;
        $product->description = $request->description ?? "";
        // $product->qty = $request->qty ?? 0;
        // $product->min_qty = $request->min_qty ?? 0;
        // $product->max_qty = $request->max_qty ?? 0;
        // $product->mrp = $request->mrp ? $request->mrp:0;
        // $product->price = $request->price ? $request->price:0;
        $product->image = $fileName;
        $product->thumbnail = $fileNameThumb;
        $product->weight = $request->weight ?? 0;
        $product->unit = $request->unit ?? 0;
        if($product->save())
        {

            if($request->product_image && count($request->product_image) > 0) {
                ProductImage::whereIn('id',$request->product_image)->delete();
            }
            if(count($images) > 0)
            {
                foreach ($images as $key => $value) {
                    $productimages = new ProductImage;
                    $productimages->product_id = $product->id;
                    $productimages->image = $value;
                    $productimages->save();
                }

            }
            
            if(count($request->price) > 0)
            {
                ProductSubunit::where(['product_id'=>$product->id])->whereNotIn('subunit_id',$request->subunit_id)->delete();
                $i = 0;
                foreach ($request->price as $key => $p_value) {
                    $productsubunit = ProductSubunit::where(['product_id'=>$product->id,'subunit_id'=>$request->subunit_id[$i]])->first();
                    if($productsubunit) {
                        $productsubunit = $productsubunit;
                    }
                    else {
                        $productsubunit = new ProductSubunit();
                    }
                    $productsubunit['product_id'] = $product->id ?? 0;
                    $productsubunit['subunit_id'] = $request->subunit_id[$i] ?? 0;
                    $productsubunit['price'] = $request->price[$i] ?? 0;
                    $productsubunit['qty'] = $request->qty[$i] ?? 0;
                    $productsubunit['mrp'] = $request->mrp[$i] ?? 0;
                    $productsubunit['min_qty'] = $request->min_qty[$i] ?? 0;
                    $productsubunit['max_qty'] = $request->max_qty[$i] ?? 0;
                    $productsubunit['color_id'] = $request->color_id[$i] ?? 0;
                    $productsubunit->save();
                    $i++;
                }
                // $product->subunits()->sync($productsubunit);

                
            }
        // $type = "Category Added Suceesfully";
        // $msg = "your";
        // $this->get_notification($type, $msg);
            // return redirect(SEGMENT.'/product?search=&category_id='.$product->category_id.'&subcategory_id='.$product->subcategory_id)->with('message','Product updated successfully.');
            return response()->json(['status' => 1, 'msg' => 'Product updated successfully']);
           
            // return redirect(SEGMENT.'/product')->with('message','Product added successfully');
        }
    }
    catch (\Throwable $e) {
        return response()->json(['status' => -1, 'msg' => $e->getMessage()]);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
       $product->is_deleted = 1;
       if($product->save()) {
          return redirect('admin/product')->with('Product deleted successfully.');
       }
       else {
          return back()->with('Product not deleted.');
       }
    }
}
