<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Cms;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas = Cms::where(['is_deleted'=>0])->get();
        return view(SEGMENT.'/cms.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view(SEGMENT.'/cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'title' => 'required',
        'slug' => 'required|unique:cms',
        'content' =>  'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $cms = new Cms;
        $cms->title = ucfirst($request->title);
        $cms->slug = strtolower($request->slug);
        $cms->content = ($request->content);
        $cms->meta_title = ucfirst($request->meta_title) ?? "";
        $cms->meta_tags = $request->meta_tags ?? "";
        $cms->meta_description = $request->meta_description ?? "";
        if($cms->save())
        {
            return redirect(SEGMENT.'/cms')->with('message','Content Added Successfully');
        }
        else
        {
            return back()->with('message','Content Not Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function show(Cms $cms)
    {
        //
        print_r($cms);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function edit(Cms $cms,$id)
    {
        //
        $cms = Cms::find($id);
        // print_r($cms);
        return view(SEGMENT.'/cms.edit',compact('cms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cms = Cms::find($id);
        $validator = validator::make($request->all(),[
        'title' => 'required',
        'content' =>  'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }

        $cms->title = ucfirst($request->title);
        $cms->content = ($request->content);
        $cms->meta_title = ucfirst($request->meta_title) ?? "";
        $cms->meta_tags = $request->meta_tags ?? "";
        $cms->meta_description = $request->meta_description ?? "";
        if($cms->save())
        {
            return redirect(SEGMENT.'/cms')->with('message','Content Updated Successfully');
        }
        else
        {
            return back()->with('message','Content Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cms $cms)
    {
        //
    }
}
