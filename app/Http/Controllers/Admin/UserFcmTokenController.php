<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DeviceToken as fcmToken;
use App\Models\Notification;
class UserFcmTokenController extends Controller
{
    /**
     *
     * @var user
     * @var FCMToken
     */
    protected $user;
    protected $fcmToken;

    /**
     * Constructor
     *
     * @param
     */
    public function __construct()
    {

    }

     /**
     * Functionality to send notification.
     *
    */

    // public function sendNotification()
    public function sendNotification($user_id,$title,$description,$type,$file=null)
    {
    	// $user_id=18;$description="Test notification from laravel ".date("d-m-Y h:i:s");$title="Test from web";$type="order";$file=NULL;
        $tokens = [];
        $apns_ids = [];
        $responseData = [];

        $this->notification_entry($user_id,$title,$description,$type);


	// for Android
        $user = User::find($user_id);
        if($user->notification == 1)
        {
            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','ANDROID')->select('device_token')->get())
            {
                foreach ($FCMTokenData as $key => $value)
                {
                    $tokens[] = $value->device_token;

                }

                $msg = array('type'=>$type,'title' => $title,'body'  => $description,'subtitle' => 'This is a subtitle','image'=>$file,
                      );

                $fields = array
                        (
                            'registration_ids'  => $tokens,
                            'notification'  => $msg
                        );


                $headers = array
                        (
                            'Authorization: key=' . env('FCM_SERVER_KEY'),
                            'Content-Type: application/json'
                        );

                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }

                $result = json_decode($result,true);

                $responseData['android'] =[
                           "result" =>$result
                        ];

                curl_close( $ch );
                 echo "<pre>";
                print_r($result);
                exit();

            }

        // for IOS

            if ($FCMTokenData = fcmToken::where('user_id',$user_id)->where('device_token','!=',null)->where('device_type','IOS')->select('device_token')->get())
            {
               foreach ($FCMTokenData as $key => $value)
                {
                    $apns_ids[] = $value->device_token;
                }

                $url = "https://fcm.googleapis.com/fcm/send";

                $serverKey = env('FCM_SERVER_KEY');
                $title = $title;
                $body = $description;
                $notification = array('type'=>$type, 'title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1','image'=>$file);
                $arrayToSend = array('registration_ids' => $apns_ids, 'notification' => $notification,'priority'=>'high');
                $json = json_encode($arrayToSend);
                $headers = array();
                $headers[] = 'Content-Type: application/json';
                $headers[] = 'Authorization: key='. $serverKey;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //Send the request
                $result = curl_exec($ch);

                if ($result === FALSE)
                {
                    die('FCM Send Error: ' . curl_error($ch));
                }
                $result = json_decode($result,true);
                // echo "<pre>";
                // print_r($result);
                // exit();
                $responseData['ios'] =[
                            "result" =>$result
                        ];

                //Close request
                curl_close($ch);
            }
        }




         return $responseData;

     }

     public function notification_entry($user_id,$title,$message,$type)
     {
        $notification = new Notification;
        $notification->user_id = $user_id;
        $notification->title = $title;
        $notification->message = $message;
        $notification->type = $type;
        $notification->save();
     }

     public function sendNotificationToAllUsers($title,$description,$type,$file=null)
     {
         $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->has('DeviceToken')->get();

         foreach ($users as $key => $value) {
            $this->sendNotification($value->id,$title,$description,$type,$file);
        }

     }
     public function sendNotificationSubscriptionPlan($title,$description,$type)
     {
        // SELECT * from users where `subscription_plan_last_date` BETWEEN "2020-12-22" and DATE_ADD("2020-12-22",INTERVAL 5 DAY)

        $users = User::where(['is_deleted'=>0,'status'=>1,'notification'=>1])->whereRaw("subscription_plan_last_date BETWEEN DATE(now()) and DATE_ADD(DATE(now()),INTERVAL 5 DAY)")->has('DeviceToken')->get();
// print_r($users);
         foreach ($users as $key => $value) {
            $this->sendNotification($value->id,$title,$description,$type);
        }

     }

}
