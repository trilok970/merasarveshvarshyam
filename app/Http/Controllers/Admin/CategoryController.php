<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Category;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use ImageResize;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo "string";dd();
         $datas = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
         return view(SEGMENT.'/category.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view(SEGMENT.'/category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        'image' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "uploads/images/".time().".$extension";
                $ff = $request->image->move(public_path('uploads/images'),$fileName);

				$thumbnail = time().".$extension";;
				$destinationPath = public_path('/uploads/thumbnail');
				$img = ImageResize::make($ff->getRealPath());
				$img->resize(200, 200, function ($constraint) {
				$constraint->aspectRatio();
				})->save($destinationPath.'/'.$thumbnail);

				$thumbnail = "uploads/thumbnail/".$thumbnail;

            }
            else
            {
                $fileName  ='default.jpg';
                $thumbnail  ='default.jpg';
            }
        $category = new Category;
        $category->name = ucfirst($request->name);
        $category->image = $fileName;
        $category->thumbnail = $thumbnail;
        if($category->save())
        {
            return redirect(SEGMENT.'/category')->with('message','Category Added Successfully');
        }
        else
        {
            return back()->with('message','Category Not Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return view(SEGMENT.'/category.edit',compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "uploads/images/".time().".$extension";
                $ff = $request->image->move(public_path('uploads/images'),$fileName);




				$thumbnail = time().".$extension";;
				$destinationPath = public_path('/uploads/thumbnail');
				$img = ImageResize::make($ff->getRealPath());
				$img->resize(200, 200, function ($constraint) {
				$constraint->aspectRatio();
				})->save($destinationPath.'/'.$thumbnail);

				$thumbnail = "uploads/thumbnail/".$thumbnail;

            }
            else
            {
                $fileName  = $category->image;
                $thumbnail = $category->thumbnail;
            }
        $category->name = ucfirst($request->name);
        $category->image = $fileName;
        $category->thumbnail = $thumbnail;

        if($category->save())
        {
            return redirect(SEGMENT.'/category')->with('message','Category Updated Successfully');
        }
        else
        {
            return back()->with('message','Category Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $category->is_deleted = 1;
        if($category->save())
        {
            return redirect(SEGMENT.'/category')->with('message','Category Deleted Successfully');
        }
        else
        {
            return back()->with('message','Category Not Deleted');
        }
    }
}
