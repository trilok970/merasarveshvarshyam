<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Audio;
use App\Models\AudioCategory;
use App\Models\MusicCategory;
use Illuminate\Http\Request;
use Validator;
use DB;

class AudioController extends Controller
{
    public $fcm;
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if($request->query())
        {
            $search = trim($request->search);
            $search = str_replace("’","'",$request->search);

            $datas = Audio::select(DB::raw("DISTINCT(audio.id)"),'audio.*')->where(['audio.is_deleted'=>0,'audio.status'=>1])
            ->leftjoin("audio_categories as au_cat",function($join){
                $join->on("au_cat.audio_id",'=','audio.id');
            })
            ->leftjoin("music_categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.music_category_id');
            })
            ->where(function($query) use ($search){
                $query->orWhere('audio.title','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('audio.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderBy('audio.order_number','asc')->paginate($this->paginate_no);
        }
        else
        {
            $datas = Audio::where(['is_deleted'=>0])->orderBy('order_number','asc')->paginate($this->paginate_no);
            $search = '';
        }


        return view(SEGMENT . '/audio.index', compact('datas','search'));
    }
    public function audio_search(Request $request)
    {
            // echo "<pre>";
            // print_r($playlistAudios);
            // exit;

        if($request->query())
        {
        $playlist = Playlist::find($request->playlist_id);
        if($playlist) {
            $playlistAudios = $playlist->PlaylistAudio->pluck('audio_id')->toArray() ?? "";
        }
        else {
            $playlistAudios = "";

        }

            $search = trim($request->search);
            $search = str_replace("’","'",$request->search);

            $datas = Audio::select(DB::raw("DISTINCT(audio.id)"),'audio.*')->where(['audio.is_deleted'=>0,'audio.status'=>1])
            ->leftjoin("audio_categories as au_cat",function($join){
                $join->on("au_cat.audio_id",'=','audio.id');
            })
            ->leftjoin("categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.music_category_id');
            })
            ->where(function($query) use ($search){
                $query->orWhere('audio.title','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('audio.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->when($playlistAudios,function($q) use ($playlistAudios) {
                return $q->whereNotIn('audio.id',$playlistAudios);
            })
            ->orderBy('audio.order_number','asc')->get();
        }
        else
        {
            $datas = Audio::where(['is_deleted'=>0])->whereNotIn('id',$playlistAudios)->orderBy('order_number','asc')->get();
            $search = '';
        }
        $data = '<table class="table table-striped table-bordered" style="width:82%;">';


                $i = 1;
                foreach($datas as $row)
                {
                    // if(in_array($row->id,$playlistAudios))
                    // $audio_check = "checked";
                    // else
                    // $audio_check = "";


                    $data .='<tr>
                    <td><input type="checkbox" name="audio_id[]" id="audio_id" value="'.$row->id.'"  /></td>
                    <td>'.$row->title.'</td>
                    <td>'.$row->tags.'</td>
                    <td>'.$row->author.'</td>
                    <td>'.$row->narrator.'</td>
                    <td><a target="_blank" href="'.$row->image.'"><img src="'.$row->image.'" width="50" /></a></td>

                   </tr> ';
                }

                $data .= '</table>';

                return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = MusicCategory::where(['is_deleted'=>0,'status'=>1])->get();

        return view(SEGMENT . '/audio.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // cho "<pre>";
        // dd($request);
        // exit;e
        //
        $validator = validator::make($request->all(), [
            'title' => 'required',
            'music_category_id' => 'required',
            'description' => 'required',
            'file' => 'required|mimes:mp3,wav,ogg|max:150000',
            'image' => 'required|mimes:jpeg,jpg,gif,png,JPEG,JPG,GIF,PNG|max:5048',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        $audio = new Audio;

        if ($request->hasFile('image') && $request->image->isValid()) {
            $image_array  =  image_upload($request->image,"uploads/images",'public');
            $audio->image = $image_array[1];
            $audio->thumbnail = $image_array[2];

        } else {
            $audio->image = 'images/default.jpg';
            $audio->thumbnail = 'images/default.jpg';
        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $fileArray        = audio_upload($request->file,"uploads/audio",'public');
            $audio->file      = $fileArray['fileName'];
            $audio->extension = $fileArray['extension'];
            $audio->duration  = $fileArray['duration'];
        }
        else {
            $audio->file = "";
            $audio->extension = "";
            $audio->duration = 0;
        }

        $audio->title = ucwords($request->title);
        $audio->description = $request->description;
        if ($audio->save()) {
            if ($request->music_category_id) {
                foreach ($request->music_category_id as $category) {
                    $audio_category = new AudioCategory();
                    $audio_category->audio_id = $audio->id;
                    $audio_category->music_category_id = $category;
                    $audio_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in audio list","audio");
            return redirect(SEGMENT . '/audio')->with('message', 'Audio added successfully');
        }
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function show(Audio $audio)
    {
        //
        $colors  = $this->colors();

        return view(SEGMENT . '/audio.show', compact('audio','colors'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function edit(Audio $audio)
    {
        $categories = MusicCategory::where(['is_deleted'=>0,'status'=>1])->get();

        return view(SEGMENT . '/audio.edit', compact('audio','categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Audio $audio)
    {



        $validator = validator::make($request->all(), [
            'title' => 'required',
            'music_category_id' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        if ($request->hasFile('image') && $request->image->isValid()) {
            $validator = validator::make($request->all(), [
                'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $image_array  =  image_upload($request->image,"uploads/images",'public');
            $audio->image = $image_array[1];
            $audio->thumbnail = $image_array[2];

        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $validator = validator::make($request->all(), [
                'file' => 'required|mimes:mp3,wav,ogg|max:150000',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }
            $fileArray        = audio_upload($request->file,"uploads/audio",'public');
            $audio->file      = $fileArray['fileName'];
            $audio->extension = $fileArray['extension'];
            $audio->duration  = $fileArray['duration'];
        }

        $audio->title = ucwords($request->title);
        $audio->description = $request->description;
        if ($audio->save()) {
            if ($request->music_category_id) {
                AudioCategory::where('audio_id',$audio->id)->delete();
                foreach ($request->music_category_id as $category) {
                    $audio_category = new AudioCategory;
                    $audio_category->audio_id = $audio->id;
                    $audio_category->music_category_id = $category;
                    $audio_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in audio list","group");
            return redirect(SEGMENT . '/audio')->with('message', 'Audio updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Audio $audio)
    {
        //
        $audio->is_deleted = 1;
        if ($audio->save()) {
            return redirect(SEGMENT . '/audio')->with('message', 'Audio deleted successfully');
        } else {
            return back()->with('message', 'Audio not deleted');
        }
    }
    public function update_order(Request $request)
    {
        $post_order = isset($_POST["post_order_ids"]) ? $_POST["post_order_ids"] : [];

        if(count($post_order)>0)
        {
            for($order_no= 0; $order_no < count($post_order); $order_no++)
            {
            if($request->type=='audio')
            $data = Audio::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='channel')
            $data = Channel::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='playlist')
            $data = Playlist::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='playlist_audio')
            $data = PlaylistAudio::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='subscriptionplan')
            $data = Subscriptionplan::where(['id'=>$post_order[$order_no]])->first();
            else if($request->type=='banner')
            $data = Banner::where(['id'=>$post_order[$order_no]])->first();
            $data->order_number = $order_no+1;
            $data->save();

            }
            echo true;
        }
        else
        {
            echo false;
        }
    }
    public function audios(Request $request)
    {
        if($request->author)
           { $val = $author = trim($request->author);$key = "Author"; }
        else
           {  $author = '';  }
        if($request->narrator)
            { $val = $narrator = trim($request->narrator); $key = "Narrator"; }
            else
            { $narrator = ''; }
        if($request->title)
            {  $val = $title = trim($request->title); $key = "Title"; }
        else
            {  $title = ''; }
        if($request->description)
            { $val = $description = trim($request->description); $key = "Description"; }
        else
            {  $description = ''; }



            $audios = Audio::where(['is_deleted'=>0])
                    ->when($author,function($q) use ($author){
                        return $q->where('author','like','%'.$author.'%');
                    })
                    ->when($narrator,function($q,$narrator){
                      return  $q->where('narrator','like','%'.$narrator.'%');
                    })
                    ->when($title,function($q,$title){
                        return $q->where('title','like','%'.$title.'%');
                    })
                    ->when($description,function($q,$description){
                        return $q->where('description','like','%'.$description.'%');
                    })
                ->orderBy('order_number','asc')->paginate($this->paginate_no);
                $colors  = $this->colors();

                return view(SEGMENT.'.audio.audios',compact('audios','colors','key','val'));

    }
}

