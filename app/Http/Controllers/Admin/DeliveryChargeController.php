<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\DeliveryCharge;
use Illuminate\Http\Request;
use Validator;
class DeliveryChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DeliveryCharge::where(['is_deleted'=>0])->orderBy('id','desc')->get();
        return view(SEGMENT.'.deliverycharge.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(SEGMENT.'.deliverycharge.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(),[
            'value' => 'required|numeric',
            'amount' => 'required|numeric',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }

            $deliverycharge = new DeliveryCharge;
            $deliverycharge->value = $request->value;
            $deliverycharge->amount = $request->amount;
            if($deliverycharge->save())
            {
                return redirect(SEGMENT.'/deliverycharge')->with('message','Delivery charges added Successfully');
            }
            else
            {
                return back()->with('message','Delivery charges not added');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DeliveryCharge  $deliveryCharge
     * @return \Illuminate\Http\Response
     */
    public function show(DeliveryCharge $deliveryCharge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DeliveryCharge  $deliveryCharge
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryCharge $deliverycharge)
    {
        return view(SEGMENT.'.deliverycharge.edit',compact('deliverycharge'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeliveryCharge  $deliveryCharge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliveryCharge $deliverycharge)
    {
        $validator = validator::make($request->all(),[
            'value' => 'required|numeric',
            'amount' => 'required|numeric',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }

            $deliverycharge->value = $request->value;
            $deliverycharge->amount = $request->amount;
            if($deliverycharge->save())
            {
                return redirect(SEGMENT.'/deliverycharge')->with('message','Delivery charges updated Successfully');
            }
            else
            {
                return back()->with('message','Delivery charges not updated');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeliveryCharge  $deliveryCharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryCharge $deliverycharge)
    {
            $deliverycharge->is_deleted = 1;
            if($deliverycharge->save())
            {
                return redirect(SEGMENT.'/deliverycharge')->with('message','Delivery charges deleted Successfully');
            }
            else
            {
                return back()->with('message','Delivery charges not deleted');
            }
    }
}
