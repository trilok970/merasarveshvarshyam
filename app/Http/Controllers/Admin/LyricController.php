<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Lyric;
use App\Models\LyricCategory;
use Illuminate\Http\Request;
use App\Models\MusicCategory;
use Validator;
use DB;
class LyricController extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if($request->query())
        {
            $search = trim($request->search);
            $search = str_replace("’","'",$request->search);

            $datas = Lyric::select(DB::raw("DISTINCT(lyrics.id)"),'lyrics.*')->where(['lyrics.is_deleted'=>0,'lyrics.status'=>1])
            ->leftjoin("lyric_categories as au_cat",function($join){
                $join->on("au_cat.lyric_id",'=','lyrics.id');
            })
            ->leftjoin("music_categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.music_category_id');
            })
            ->where(function($query) use ($search){
                $query->orWhere('lyrics.title','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('lyrics.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderBy('lyrics.order_number','asc')->paginate($this->paginate_no);
        }
        else
        {
            $datas = lyric::where(['is_deleted'=>0])->orderBy('order_number','asc')->paginate($this->paginate_no);
            $search = '';
        }


        return view(SEGMENT . '/lyric.index', compact('datas','search'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = MusicCategory::where(['is_deleted'=>0,'status'=>1])->get();

        return view(SEGMENT . '/lyric.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // cho "<pre>";
        // dd($request);
        // exit;e
        //
        $validator = validator::make($request->all(), [
            'title' => 'required',
            'music_category_id' => 'required',
            'description' => 'required',
            'file' => 'required|max:150000',
            'image' => 'required|mimes:jpeg,jpg,gif,png,JPEG,JPG,GIF,PNG|max:5048',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        $lyric = new Lyric;

        if ($request->hasFile('image') && $request->image->isValid()) {

            $image_array  =  image_upload($request->image,"uploads/images",'public');
            $lyric->image = $image_array[1];
            $lyric->thumbnail = $image_array[2];

        } else {
            $lyric->image = 'images/default.jpg';
            $lyric->thumbnail = 'images/default.jpg';
        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $fileArray = lyric_upload($request->file,"uploads/lyric",'public');
            // echo "<pre>";
            // print_r($fileArray);exit;
            if($fileArray[0] == 1) {
                $lyric->file      = $fileArray[1];
                $lyric->extension = $fileArray[2];
            }
            else {
                $validator = $validator->errors()->add("file",' The file must be a file of type: lrc, LRC.');
                return back()
                        ->withInput()
                        ->withErrors($validator);
            }
        }
        else {
            $lyric->file = "";
            $lyric->extension = "";
            $lyric->duration = 0;
        }

        $lyric->title = ucwords($request->title);
        $lyric->description = $request->description;
        if ($lyric->save()) {
            if ($request->music_category_id) {
                foreach ($request->music_category_id as $category) {
                    $lyric_category = new LyricCategory();
                    $lyric_category->lyric_id = $lyric->id;
                    $lyric_category->music_category_id = $category;
                    $lyric_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in lyric list","lyric");
            return redirect(SEGMENT . '/lyric')->with('message', 'Lyric added successfully');
        }
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function show(Lyric $lyric)
    {
        //
        $colors  = $this->colors();

        return view(SEGMENT . '/lyric.show', compact('lyric','colors'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function edit(Lyric $lyric)
    {
        $categories = MusicCategory::where(['is_deleted'=>0,'status'=>1])->get();

        return view(SEGMENT . '/lyric.edit', compact('lyric','categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lyric $lyric)
    {



        $validator = validator::make($request->all(), [
            'title' => 'required',
            'music_category_id' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        if ($request->hasFile('image') && $request->image->isValid()) {
            $validator = validator::make($request->all(), [
                'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $image_array  =  image_upload($request->image,"uploads/images",'public');
            $lyric->image = $image_array[1];
            $lyric->thumbnail = $image_array[2];

        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $validator = validator::make($request->all(), [
                'file' => 'required|max:150000',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $fileArray = lyric_upload($request->file,"uploads/lyric",'public');
            // echo "<pre>";
            // print_r($fileArray);exit;
            if($fileArray[0] == 1) {
                $lyric->file      = $fileArray[1];
                $lyric->extension = $fileArray[2];
            }
            else {
                $validator = $validator->errors()->add("file",' The file must be a file of type: lrc, LRC.');
                return back()
                        ->withInput()
                        ->withErrors($validator);
            }
        }

        $lyric->title = ucwords($request->title);
        $lyric->description = $request->description;
        if ($lyric->save()) {
            if ($request->music_category_id) {
                LyricCategory::where('lyric_id',$lyric->id)->delete();
                foreach ($request->music_category_id as $category) {
                    $lyric_category = new LyricCategory;
                    $lyric_category->lyric_id = $lyric->id;
                    $lyric_category->music_category_id = $category;
                    $lyric_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in lyric list","group");
            return redirect(SEGMENT . '/lyric')->with('message', 'Lyric updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lyric $lyric)
    {
        //
        $lyric->is_deleted = 1;
        if ($lyric->save()) {
            return redirect(SEGMENT . '/lyric')->with('message', 'Lyric deleted successfully');
        } else {
            return back()->with('message', 'Lyric not deleted');
        }
    }

}
