<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Color;
use Illuminate\Http\Request;
use Validator;
class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Color::where(['is_deleted'=>0])->get();
        return view('admin.color.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = validator::make($request->all(),[
            'code' => 'required',
            ]);
            if($validator->fails())
            {
                return response()->json(['status' => 0, 'errors' => $validator->errors()]);
            }
            
            $color = new Color();
            $color->code = $request->code;
            if($color->save())
            {
                return response()->json(['status' => 1, 'msg' => 'Color added successfully']);
            }
            else
            {
                return response()->json(['status' => -1, 'msg' => 'Color not added']);
            }
        }
        catch (\Throwable $e) {
            return response()->json(['status' => -1, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        return view('admin.color.edit',compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        try {
            $validator = validator::make($request->all(),[
            'code' => 'required',
            ]);
            if($validator->fails())
            {
                return response()->json(['status' => 0, 'errors' => $validator->errors()]);
            }
            
            $color->code = $request->code;
            if($color->save())
            {
                return response()->json(['status' => 1, 'msg' => 'Color added successfully']);
            }
            else
            {
                return response()->json(['status' => -1, 'msg' => 'Color not added']);
            }
        }
        catch (\Throwable $e) {
            return response()->json(['status' => -1, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        $color->is_deleted = 1;
        if($color->save()) {
            return redirect('admin/color')->with('Color deleted successfully.');
        }
        else {
            return back()->with('Color not deleted.');
        }
    }
}
