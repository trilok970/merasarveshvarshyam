<?php

namespace App\Http\Controllers;

use App\Models\AudioCategory;
use Illuminate\Http\Request;

class AudioCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AudioCategory  $audioCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AudioCategory $audioCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AudioCategory  $audioCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AudioCategory $audioCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AudioCategory  $audioCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AudioCategory $audioCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AudioCategory  $audioCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AudioCategory $audioCategory)
    {
        //
    }
}
