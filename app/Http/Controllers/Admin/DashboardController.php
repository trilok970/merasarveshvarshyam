<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Admin;
use App\Models\Category;
use App\Models\Cms;
use App\Models\MusicCategory;
use App\Models\Order;
use App\Models\Product;
use App\Models\Subcategory;
use App\Models\User;
use Auth;
use DB;
use Session;
use Hash;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['user_count'] = User::where(['is_deleted'=>0])->count();
        $data['product_count'] = Product::where(['is_deleted'=>0])->count();
        $data['category_count'] = Category::where(['is_deleted'=>0])->count();
        $data['subcategory_count'] = Subcategory::where(['is_deleted'=>0])->count();
        $data['musiccategory_count'] = MusicCategory::where(['is_deleted'=>0])->count();
        $data['order_count'] = Order::where(['status'=>'complete'])->count();
        return view(SEGMENT.'/dashboard.index',compact('data'));
    }
    public function myProfile()
    {
        $user = Auth::guard('admin')->user();

        return view(SEGMENT.'.dashboard.my-profile',compact('user'));
    }
    public function editProfile()
    {
        $user = Auth::guard('admin')->user();
        return view(SEGMENT.'.dashboard.edit-profile',compact('user'));
    }
    public function editProfilePost(Request $request)
    {
        $credentials = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
        ]);
        $user = Auth::guard('admin')->user();
        if($request->hasFile('profile_pic') && $request->profile_pic->isValid())
        {
            $image_array  =  image_upload($request->profile_pic,"uploads/images",'public');
            $user->profile_pic = $image_array[1];
        }


        $user->name = ucwords($request->name);
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->save();
        return redirect(SEGMENT.'/dashboard')->with('message','Profile updated successfully.');
    }
    public function createTable()
    {

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->foreignId('user_id')->nullable();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload');
            $table->integer('last_activity');
        });


        // Schema::dropIfExists('jobs');



    }
    public function privacy_policy()
    {
        $page_title = "Privacy Policy";
        $cms = Cms::where('slug','privacy-policy')->first();
        return view('privacy-policy',compact('cms','page_title'));

    }
    public function check_mail($email)
    {


       Mail::to($email)->send(new UserMail());
        if (Mail::failures())
        {
            echo  "response showing failed emails";
        }

           echo "Email sent successfully";


    }
    public function changePassword()
    {
        $user = Auth::guard('admin')->user();

        return view('admin.dashboard.change-password',compact('user'));
    }
    public function changePasswordPost(Request $request)
    {
        $user = Auth::guard('admin')->user();

        $request->validate([

            'current_password' => ['required'],

            'new_password' => ['required'],

            'password_confirmation' => ['same:new_password'],

        ]);

        if(!Hash::check($request->current_password, $user->password))
        {
            return back()->with('error_message','Current password does not match.');
        }
        else
        {
            Admin::find($user->id)->update(['password'=> Hash::make($request->new_password)]);
            return redirect(SEGMENT.'/dashboard')->with('message','Password successfully changed.');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
