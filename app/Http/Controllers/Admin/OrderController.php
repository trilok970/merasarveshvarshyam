<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderCancel;
use App\Models\OrderShipping;
use App\Models\OrderDelivery;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\UserFcmTokenController;

class OrderController extends Controller
{
    public $fcm;

	public function __construct()
    {
        $this->fcm = new UserFcmTokenController;

    }
    public function orders($id=null)
	{
			if($id==null)
			{
			$order_status = "all";
			$orders = Order::where(['status'=>'complete'])->orderBy('id','desc')->with('user')->get();
			}
			elseif($id=="failed" || $id=="complete")
			{
			$order_status = $id;
			$orders = Order::where(['status'=>$id])->orderBy('id','desc')->with('user')->get();
			}
			else
			{
			$order_status = $id;
			$orders = Order::where(['order_status'=>$id,'status'=>'complete'])->orderBy('id','desc')->with('user')->get();
			}

        return view(SEGMENT.'.order.index',compact('orders','order_status'));
    }
    public function update_order_status($order_id,$user_id,$order_status,Request $request)
    {
        //echo $order_id;exit;
        $backurl = url()->previous();
        $order = Order::where('id',$order_id)->first();
        if($order)
        {
        $order->order_status = $order_status;
        $order->save();
        if($order_status=="received")
        {
            $msg = "Order No - ".$order->order_no." <br>Your order is successfully received. You order will  be dispatched after 1-2 business days.";
            //send notification and message
            $title="Order Received";
            // $res = $this->send_pre_msg($user->mobile_no,$msg);

        }
        else if($order_status=="canceled")
        {
            $msg = "Order No - ".$order->order_no." <br>Your order is canceled by Admin. The reason of cancel-<br/>".$request->comment;

            $ordercancel = new OrderCancel();
            $ordercancel->user_id  = $user_id;
            $ordercancel->order_id = $order_id;
            $ordercancel->comment  = $request->comment;
            $ordercancel->save();
            //send notification and message
            $title="Order Canceled";
            // $txtmsg = "Order No - ".$order_id." <br>Your order is canceled by Admin. The reason of cancel - ".$request->comment;
            // $res = $this->send_pre_msg($user->mobile_no,$txtmsg);
        }
        else if ($order_status=="shipped")
        {
            $msg = "Order No - ".$order->order_no." <br>Your order is successfully shipped . Your shipping details is ....<br/>Tracking Id - ".$request->tracking_id."<br/>Shipping Details - ".$request->shipping_details;

            $ordershipping = new OrderShipping();
            $ordershipping->user_id  = $user_id;
            $ordershipping->order_id = $order_id;
            $ordershipping->tracking_id  = $request->tracking_id;
            $ordershipping->shipping_details = $request->shipping_details;
            $ordershipping->save();
            //send notification and message

            $description = "Your order is successfully shipped . Your shipping details is ....Tracking Id - ".$request->tracking_id."Shipping Details - ".$request->shipping_details;
            $title="Order Shipped";
            // $txtmsg = "Order No - ".$order_id." <br>Your order is successfully shipped . Your shipping details is ....<br>Tracking Id - ".$request->tracking_id."<br>Shipping Details - ".$request->shipping_details;
            // $res = $this->send_pre_msg($user->mobile_no,$txtmsg);
        }

        //$this->send_user_email($user->email,$msg,$user->name,$title);
            $this->fcm->sendNotification($order->user_id,$title,$msg,"order");
            return redirect($backurl)->with('message','Order Status updated successfully');
        }
        else{
        return redirect($backurl)->with('message','Something went wrong');
        }
    }
    public function update_order_status1(Request $request)
    {
        //echo $order_id;exit;

        $backurl = url()->previous();
        $order = Order::where('id',$request->order_id)->first();
        if($order)
        {
        $order->order_status = $request->order_status;
        $order->save();
        if ($request->order_status=="delivered")
        {
            $msg = "Order No - ".$order->order_no." <br>Your order is successfully delivered . Your delivery details is ....<br/>Delivery Details - ".$request->additional_details;

            if($request->hasFile('delivery_proof') && $request->delivery_proof->isValid())
            {
                   $extension = $request->delivery_proof->extension();
                   $fileName  = "uploads/images/".time().".$extension";
                   $request->delivery_proof->move(public_path('uploads/images'),$fileName);
            }
            else
            {
                   $fileName = "uploads/images/default.jpg";
            }
            $orderdelivery = new OrderDelivery();
            $orderdelivery->user_id  = $request->user_id;
            $orderdelivery->order_id = $request->order_id;
            $orderdelivery->delivery_proof  = $fileName;
            $orderdelivery->additional_details = $request->additional_details;
            $orderdelivery->save();

            //send notification and message
            $title="Order Delivered";

            // $txtmsg = "Order No - ".$order->order_id." <br>Your order is successfully delivered . Your delivery details is ....<br>Delivery Details - ".$request->additional_details;
            // $res = $this->send_pre_msg($user->mobile_no,$txtmsg);

        }
        else if ($request->order_status=="shipped")
        {
            $msg = "Order No - ".$order->order_no." <br>Your order is successfully shipped . Your shipping details is ....<br/>Tracking Id - ".$request->tracking_id."<br/>Shipping Details - ".$request->shipping_details;

            if($request->hasFile('delivery_proof') && $request->delivery_proof->isValid())
            {
                   $extension = $request->delivery_proof->extension();
                   $fileName  = "uploads/images/".time().".$extension";
                   $request->delivery_proof->move(public_path('uploads/images'),$fileName);
            }
            else
            {
                   $fileName = "uploads/images/default.jpg";
            }
            $ordershipping = new OrderShipping();
            $ordershipping->user_id  = $request->user_id;
            $ordershipping->order_id = $request->order_id;
            $ordershipping->tracking_id  = $request->tracking_id;
            $ordershipping->shipping_details = $request->shipping_details;
            $ordershipping->delivery_proof  = $fileName;
            $ordershipping->save();

            //send notification and message
            $title="Order Shipped";
            $description = "Your order is successfully shipped . Your shipping details is ....Tracking Id - ".$request->tracking_id."Shipping Details - ".$request->shipping_details;

            // $txtmsg = "Order No - ".$order->order_id." <br>Your order is successfully shipped . Your shipping details is ....<br>Tracking Id - ".$request->tracking_id."<br>Shipping Details - ".$request->shipping_details;
            // $res = $this->send_pre_msg($user->mobile_no,$txtmsg);
        }

        //$this->send_user_email($user->email,$msg,$user->name,$title);
        $this->fcm->sendNotification($order->user_id,$title,$msg,"order");
        return redirect($backurl)->with('message','Order Status updated successfully');
        }
        else{
        return redirect($backurl)->with('message','Something went wrong');
        }
    }
    public function show_order_invoice($id)
    {
        $order = Order::where('id',$id)->first();
        // echo "<pre>";
        // print_r($order->OrderDetail);exit;
        return view('admin.order.show-order-invoice',compact('order'));

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order_id = $order->id;
        $order_details = OrderDetail::where('id',$order_id)->get();




		// echo "<pre>";
		// print_r($order->OtherAddress);exit;
        $address = ($order->user->fullname)."<br> ".$order->OtherAddress->apartment .' '.$order->OtherAddress->street
                    .'<br>'.$order->OtherAddress->location
                    .'<br>'.$order->OtherAddress->pincode
                    .'<br>'.$order->OtherAddress->phone_number;
        $total_tax = 0;

		$msg = '<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:16px;">
		  <tr>
			<td  height="30" style="border-bottom:1px solid #CCCCCC;">Order Number: <strong>'.$order->order_no.'</strong> </td>
			<td height="30" style="border-bottom:1px solid #CCCCCC;">Order Date: <strong>'.date("d-M-Y h:i A",strtotime($order->created_at)).'</strong> </td>
			<td  align="right" style="border-bottom:1px solid #CCCCCC;">Order Status: <b>'.ucfirst($order->order_status).'</b> </td>
		  </tr>
		</table>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
		  <tr>
			<td width="335" valign="top" style="border-right:1px solid #CCCCCC; padding-top:12px; padding-bottom:12px;"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
        <td width="25%" valign="top"><strong>Bill To: </strong></td>
        <td width="75%" valign="top"><strong>'.ucwords($order->user->name).'</strong>
		  </td>
      </tr>
			  <tr>
        <td width="25%" valign="top">&nbsp;</td>
        <td width="75%" valign="top">'.ucwords($address).'
		  </td>
      </tr>

    </table></td>
    <td width="335" valign="top" style="padding-top:12px; padding-bottom:12px;"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
        <td width="25%" valign="top"><strong>Ship To: </strong></td>
        <td width="75%" valign="top"><strong>'.ucwords($order->user->name).'</strong>
		  </td>
      </tr>
      <tr>
        <td width="25%" valign="top">&nbsp;</td>
        <td width="75%" valign="top">'.ucwords($address).'</td>
      </tr>
    </table></td>
  </tr>
</table>
		';


$msg .= '<table width="100%" border="0" align="center" cellpadding="6" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
  <tr>
    <td width="375" align="left" style="background-color:#000000; color:#FFFFFF;padding:20xp;font-size:14px;"><strong>Product Description</strong></td>
    <td width="80" align="center" style="background-color:#000000; color:#FFFFFF;padding:20xp;font-size:14px;"><strong>Qty.</strong></td>
    <td width="111" align="right" style="background-color:#000000; color:#FFFFFF;padding:20xp;font-size:14px;"><strong>Unit Price</strong></td>
	<td width="80" align="right" style="background-color:#000000; color:#FFFFFF;padding:20xp;font-size:14px;"><strong>Tax</strong></td>
    <td width="150" align="right" style="background-color:#000000; color:#FFFFFF;padding:20xp;font-size:14px;"><strong>Total Price</strong></td>
  </tr>';

$t_price = 0;
foreach($order->OrderDetail as $order_detail)
{

$msg .= '<tr>
    <td align="left" style="border-bottom:1px solid #CCCCCC;">'.$order_detail->product_name.'</td>
    <td align="center" style="border-bottom:1px solid #CCCCCC;">';
	if($order_detail->qty==0)
	{
		$type=ucfirst($order_detail->type);
	}
	else
		$type=$order_detail->qty;
	$msg .= ''.$type.'</td>
    <td align="right" style="border-bottom:1px solid #CCCCCC;">';
	$price=$order->price;
	$t_price +=$order_detail->price*$order_detail->qty;
	$total_tax +=$order_detail->tax*$order_detail->qty;

	$msg .= ''.number_format($order_detail->price,2).'</td>
	<td align="right" style="border-bottom:1px solid #CCCCCC;">'.number_format($order_detail->tax,2).'</td>
    <td align="right" style="border-bottom:1px solid #CCCCCC;">'.number_format($order_detail->total_price,2).'</td>
  </tr>';

}



$msg .= '<tr>
    <td align="center">&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right" colspan="2">Total</td>
    <td align="right">'.number_format(($t_price),2).'</td>
  </tr>

  <tr>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="right" colspan="2">Total Tax </td>
    <td align="right">'.number_format($total_tax,2).'</td>
  </tr>

  <tr>

    <td align="center" bgcolor="#CCCCCC">&nbsp;</td>
    <td align="center" bgcolor="#CCCCCC">&nbsp;</td>
    <td align="right" bgcolor="#CCCCCC" colspan="2"><strong>Grand Total</strong></td>
    <td align="right" bgcolor="#CCCCCC"><strong>'.number_format(round($t_price+$total_tax),2).'</strong></td>
  </tr>
 ';

  $msg .= '
</table>';


echo $msg;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
