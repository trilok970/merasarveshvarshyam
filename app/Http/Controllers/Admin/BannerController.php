<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Banner;
use Illuminate\Http\Request;
use Validator;
use ImageResize;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->query())
        {
           $banners = Banner::where(function($q) use ($request) {
           $name=$request->query('search');
            $q->orWhere('id','like','%'.$request->query('search').'%');
            $q->orWhere('title','like','%'.$request->query('search').'%');
            $q->orWhere('user_id','like', '%'.$request->query('search').'%');
            })->orderBy('id')->paginate(50);
            return view('admin.banner.index',compact('banners'))->with('search',$request->query('search'));
        }
        else
        {
            $banners = Banner::where('is_deleted','0')->orderBy('id','desc')->get();
            return view('admin.banner.index',compact('banners'))->with('search','');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = validator::make($request->all(),[
        'title' => 'required',
        'description' => 'required',
        'image' => 'required',
        'thumbnail' => 'required',

        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $banner = new Banner    ;
          if($request->hasFile('image') && $request->image->isValid())
          {
            $extension = $request->image->extension();
            $fileName  = "uploads/images/". time().".$extension";
            $ff = $request->image->move(public_path('uploads/images'),$fileName);

           $thumbnail = time().".$extension";;
                    $destinationPath = public_path('/uploads/images');
                    $img1 = ImageResize::make($ff->getRealPath());
                    $img1->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);
          }
          else
          {
                $fileName = "uploads/images/default.jpg";
          }
           if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
          {
            $extension1 = $request->thumbnail->extension();
            $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
            $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);

           $thumbnail1 = time().".$extension1";;
                    $destinationPath1 = public_path('/uploads/thumbnail');
                    $img1 = ImageResize::make($ff1->getRealPath());
                    $img1->orientate()
                    ->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath1.'/'.$thumbnail1);
          }
          else
          {
                $fileNameThumb = "uploads/images/default.jpg";
          }


            $banner->title          = ucwords($request->title);
            $banner->type           = "banner";
            $banner->description    = $request->description;
            $banner->thumbnail      = $fileNameThumb;
            $banner->image          = $fileName;
            $banner->save();
            return redirect('admin/banner')->with('message','Banner is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('admin.banner.edit',compact('banner'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
         $validator = validator::make($request->all(),[
        'title' => 'required',
        'description' => 'required',

        ]);
        if($validator->fails())
        {
            return redirect('admin/banner/create')
            ->withInput()
            ->withErrors($validator);
        }
          if($request->hasFile('image') && $request->image->isValid())
          {
            $extension = $request->image->extension();
            $fileName  = "uploads/images/". time().".$extension";
            $ff = $request->image->move(public_path('uploads/images'),$fileName);

           $thumbnail = time().".$extension";;
                    $destinationPath = public_path('/uploads/images');
                    $img1 = ImageResize::make($ff->getRealPath());
                    $img1->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);
          }
          else
          {
                $fileName = $banner->image;
          }
           if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
          {
            $extension1 = $request->thumbnail->extension();
            $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
            $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);

           $thumbnail1 = time().".$extension1";;
                    $destinationPath1 = public_path('/uploads/thumbnail');
                    $img1 = ImageResize::make($ff1->getRealPath());
                    $img1->orientate()
                    ->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath1.'/'.$thumbnail1);
          }
          else
          {
                $fileNameThumb = $banner->thumbnail;
          }


            $banner->title          = ucwords($request->title);
            $banner->type           = "banner";
            $banner->description    = $request->description;
            $banner->thumbnail      = $fileNameThumb;
            $banner->image          = $fileName;
            $banner->save();
            return redirect('admin/banner')->with('message','Banner is updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
         if($banner)
        {
            $banner->is_deleted = 1;
            $banner->save();
            return back()->with('message','Banner deleted successfully');
        }
        else{
            return back()->with('message','Something wrong');
        }
    }
}
