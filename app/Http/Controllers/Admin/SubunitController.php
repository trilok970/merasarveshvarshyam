<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subunit;
use App\Models\Unit;
use Illuminate\Http\Request;
use Validator;
class SubunitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Subunit::where(['is_deleted'=>0])->with('unit')->orderBy('id','desc')->get();
        return view(SEGMENT.'/subunit.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $units = Unit::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view(SEGMENT.'/subunit.create',compact('units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(),[
            'unit_id' => 'required',
            'name' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }
            
            $subunit = new Subunit();
            $subunit->name = ucfirst($request->name);
            $subunit->unit_id = $request->unit_id;
            if($subunit->save())
            {
                return redirect(SEGMENT.'/subunit')->with('message','Subunit added successfully');
            }
            else
            {
                return back()->with('message','Subunit not added');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subunit $subunit)
    {
        $units = Unit::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view(SEGMENT.'/subunit.edit',compact('subunit','units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subunit $subunit)
    {
        $validator = validator::make($request->all(),[
            'unit_id' => 'required',
            'name' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }
           
            $subunit->name = ucfirst($request->name);
            $subunit->unit_id = $request->unit_id;
    
            if($subunit->save())
            {
                return redirect(SEGMENT.'/subunit')->with('message','Subunit updated successfully');
            }
            else
            {
                return back()->with('message','Subunit not updated');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subunit $subunit)
    {
        $subunit->is_deleted = 1;
        if($subunit->save())
        {
            return redirect(SEGMENT.'/subunit')->with('message','Subunit deleted successfully');
        }
        else
        {
            return back()->with('message','Subunit not deleted');
        }
    }
}
