<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\MusicCategory;
use Illuminate\Http\Request;
use Validator;
use DB;
use ImageResize;
class MusicCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MusicCategory::where(['is_deleted'=>0])->orderByDesc('id')->get();
         return view(SEGMENT.'/musiccategory.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(SEGMENT.'/musiccategory.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make($request->all(),[
            'name' => 'required',
            'image' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }
            if($request->hasFile('image') && $request->image->isValid())
               {
                    $extension = $request->image->extension();
                    $fileName  = "uploads/images/".time().".$extension";
                    $ff = $request->image->move(public_path('uploads/images'),$fileName);

                    $thumbnail = time().".$extension";;
                    $destinationPath = public_path('/uploads/thumbnail');
                    $img = ImageResize::make($ff->getRealPath());
                    $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);

                    $thumbnail = "uploads/thumbnail/".$thumbnail;

                }
                else
                {
                    $fileName  ='default.jpg';
                    $thumbnail  ='default.jpg';
                }
            $musiccategory = new MusicCategory;
            $musiccategory->name = ucfirst($request->name);
            $musiccategory->image = $fileName;
            $musiccategory->thumbnail = $thumbnail;
            if($musiccategory->save())
            {
                return redirect(SEGMENT.'/musiccategory')->with('message','Music category added successfully');
            }
            else
            {
                return back()->with('message','Music category not added');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MusicCategory  $musicCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MusicCategory $musiccategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MusicCategory  $musicCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(MusicCategory $musiccategory)
    {
        return view(SEGMENT.'/musiccategory.edit',compact('musiccategory'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MusicCategory  $musicCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MusicCategory $musiccategory)
    {
        $validator = validator::make($request->all(),[
            'name' => 'required',
            ]);
            if($validator->fails())
            {
                return back()
                ->withInput()
                ->withErrors($validator);
            }
            if($request->hasFile('image') && $request->image->isValid())
               {
                    $extension = $request->image->extension();
                    $fileName  = "uploads/images/".time().".$extension";
                    $ff = $request->image->move(public_path('uploads/images'),$fileName);




                    $thumbnail = time().".$extension";;
                    $destinationPath = public_path('/uploads/thumbnail');
                    $img = ImageResize::make($ff->getRealPath());
                    $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$thumbnail);

                    $thumbnail = "uploads/thumbnail/".$thumbnail;

                }
                else
                {
                    $fileName  = $musiccategory->image;
                    $thumbnail = $musiccategory->thumbnail;
                }
            $musiccategory->name = ucfirst($request->name);
            $musiccategory->image = $fileName;
            $musiccategory->thumbnail = $thumbnail;

            if($musiccategory->save())
            {
                return redirect(SEGMENT.'/musiccategory')->with('message','Music category updated successfully');
            }
            else
            {
                return back()->with('message','Music category not updated');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MusicCategory  $musicCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MusicCategory $musiccategory)
    {
        $musiccategory->is_deleted = 1;
        if($musiccategory->save())
        {
            return redirect(SEGMENT.'/musiccategory')->with('message','Music category deleted successfully');
        }
        else
        {
            return back()->with('message','Music category not deleted');
        }
    }
}
