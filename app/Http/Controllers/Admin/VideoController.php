<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Models\MusicCategory;
use App\Models\VideoCategory;
use DB;
use Validator;
class VideoController extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
        $this->api_per_page = config('constants.api_per_page');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if($request->query())
        {
            $search = trim($request->search);
            $search = str_replace("’","'",$request->search);

            $datas = Video::select(DB::raw("DISTINCT(videos.id)"),'videos.*')->where(['videos.is_deleted'=>0,'videos.status'=>1])
            ->leftjoin("video_categories as au_cat",function($join){
                $join->on("au_cat.video_id",'=','videos.id');
            })
            ->leftjoin("music_categories as cat",function($join){
                $join->on("cat.id",'=','au_cat.music_category_id');
            })
            ->where(function($query) use ($search){
                $query->orWhere('videos.title','like','%'.$search.'%');
                $query->orWhere('cat.name','like','%'.$search.'%');
                $query->orWhere('videos.created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderBy('videos.order_number','asc')->paginate($this->paginate_no);
        }
        else
        {
            $datas = Video::where(['is_deleted'=>0])->orderBy('order_number','asc')->paginate($this->paginate_no);
            $search = '';
        }


        return view(SEGMENT . '/video.index', compact('datas','search'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = MusicCategory::where(['is_deleted'=>0,'status'=>1])->get();

        return view(SEGMENT . '/video.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // cho "<pre>";
        // dd($request);
        // exit;e
        //
        $validator = validator::make($request->all(), [
            'title' => 'required',
            'music_category_id' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpeg,jpg,gif,png,JPEG,JPG,GIF,PNG|max:5048',
        ]);
        $validator->sometimes(['youtube_url'], 'required', function ($request) {
            return $request->type === 'Youtube';
        });
        $validator->sometimes(['file'], 'required|mimes:mp4,MP4,mov,MOV,wmv,WMV,avi,AVI,mkv,MKV,flv,FLV,mpeg-2,MPEG-2|max:150000', function ($request) {
            return $request->type === 'File';
        });
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        $video = new video;

        if ($request->hasFile('image') && $request->image->isValid()) {
            $image_array  =  image_upload($request->image,"uploads/images",'public');
            $video->image = $image_array[1];
            $video->thumbnail = $image_array[2];

        } else {
            $video->image = 'images/default.jpg';
            $video->thumbnail = 'images/default.jpg';
        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $fileArray        = video_upload($request->file,"uploads/video",'public');
            // echo "<pre>";
            // print_r($fileArray);exit;
            $video->file      = $fileArray[1];
            $video->extension = $fileArray[4];
            $video->duration  = $fileArray[3];
            // $video->thumbnail  = $fileArray[2];
        }
        else {
            $video->file = "";
            $video->extension = "";
            $video->duration = 0;
        }

        $video->title = ucwords($request->title);
        $video->description = $request->description;
        $video->type = $request->type;
        $video->youtube_url = $request->youtube_url ?? "";
        if ($video->save()) {
            if ($request->music_category_id) {
                foreach ($request->music_category_id as $category) {
                    $video_category = new VideoCategory();
                    $video_category->video_id = $video->id;
                    $video_category->music_category_id = $category;
                    $video_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in video list","video");
            return redirect(SEGMENT . '/video')->with('message', 'Video added successfully');
        }
    }
    public function colors()
    {
     return $color = array("success","danger","warning","info","light","dark","secondary");
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
        $colors  = $this->colors();

        return view(SEGMENT . '/video.show', compact('video','colors'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        $categories = MusicCategory::where(['is_deleted'=>0,'status'=>1])->get();

        return view(SEGMENT . '/video.edit', compact('video','categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {



        $validator = validator::make($request->all(), [
            'title' => 'required',
            'music_category_id' => 'required',
            'description' => 'required',
        ]);
        $validator->sometimes(['youtube_url'], 'required', function ($request) {
            return $request->type === 'Youtube';
        });
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        if ($request->hasFile('image') && $request->image->isValid()) {
            $validator = validator::make($request->all(), [
                'image' => 'required|mimes:jpeg,jpg,gif,png|max:2048',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $image_array  =  image_upload($request->image,"uploads/images",'public');
            $video->image = $image_array[1];
            $video->thumbnail = $image_array[2];

        }
        if ($request->hasFile('file') && $request->file->isValid()) {
            $validator = validator::make($request->all(), [
                'file' => 'required|mimes:mp4,MP4,mov,MOV,wmv,WMV,avi,AVI,mkv,MKV,flv,FLV,mpeg-2,MPEG-2|max:150000',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $fileArray  =  video_upload($request->file,"video",'public');
            $video->file  = $fileArray['fileName'];
            $video->extension = $fileArray['extension'];
            $video->duration  = $fileArray['duration'];
        }

        $video->title = ucwords($request->title);
        $video->description = $request->description;
        $video->type = $request->type;
        $video->youtube_url = $request->youtube_url ?? "";
        if ($video->save()) {
            if ($request->music_category_id) {
                VideoCategory::where('video_id',$video->id)->delete();
                foreach ($request->music_category_id as $category) {
                    $video_category = new VideoCategory;
                    $video_category->video_id = $video->id;
                    $video_category->music_category_id = $category;
                    $video_category->save();
                }
            }
            // Send notification to all users
            // $this->fcm->sendNotificationToAllUsers("New song added","1 new song added in video list","group");
            return redirect(SEGMENT . '/video')->with('message', 'Video updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
        $video->is_deleted = 1;
        if ($video->save()) {
            return redirect(SEGMENT . '/video')->with('message', 'Video deleted successfully');
        } else {
            return back()->with('message', 'Video not deleted');
        }
    }


}
