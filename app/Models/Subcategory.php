<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    //

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
}
