<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;
    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
}
