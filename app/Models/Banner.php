<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
}
