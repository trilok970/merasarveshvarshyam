<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioCategory extends Model
{
    use HasFactory;
    public function audio() {
        return $this->belongsTo(Audio::class);
    }
    public function music_category() {
        return $this->belongsTo(MusicCategory::class);
    }
}
