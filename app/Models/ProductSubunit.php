<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSubunit extends Model
{
    use HasFactory;
    protected $appends = ['subunit_value','color_value'];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function subunit()
    {
        return $this->belongsTo(Subunit::class);
    }
    public function getSubunitValueAttribute() {
        return Subunit::find($this->subunit_id)->name ?? "";
    }
    public function getColorValueAttribute() {
        return Color::find($this->color_id)->code ?? "";
    }
}
