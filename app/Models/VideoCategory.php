<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model
{
    use HasFactory;
    public function video() {
        return $this->belongsTo(Video::class);
    }
    public function music_category() {
        return $this->belongsTo(MusicCategory::class);
    }
}
