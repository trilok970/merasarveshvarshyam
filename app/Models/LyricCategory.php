<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LyricCategory extends Model
{
    use HasFactory;
    public function lyric() {
        return $this->belongsTo(Lyric::class);
    }
    public function music_category() {
        return $this->belongsTo(MusicCategory::class);
    }
}
