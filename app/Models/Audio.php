<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    use HasFactory;
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getFileAttribute($value) {
        return file_checker($value, 'audio');
    }
    public function AudioCategory() {
        return $this->hasMany(AudioCategory::class);
    }



}
