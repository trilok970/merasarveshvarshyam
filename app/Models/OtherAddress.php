<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherAddress extends Model
{
    use HasFactory;
     protected $fillable = [
        'name', 'pincode', 'phone_number','other_phone_number','city_id','area','landmark','user_id','building_name','state_id','status','is_deleted','address_type','location','street','apartment','lat','lng'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function state()
    {
    	return $this->belongsTo(State::class);
    }
    public function city()
    {
    	return $this->belongsTo(City::class);
    }
}
