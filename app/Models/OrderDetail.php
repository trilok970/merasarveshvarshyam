<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
     public function order()
    {
    	return $this->belongsTo(Order::class);
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
}
