<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getFileAttribute($value) {
        return file_checker($value, 'video.mp4');
    }
    public function VideoCategory() {
        return $this->hasMany(VideoCategory::class);
    }
}
