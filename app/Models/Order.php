<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $appends = ['show-order-invoice'];

    public function getShowOrderInvoiceAttribute()
    {
        return url('show-order-invoice/'.$this->id);
    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function OrderDetail()
    {
    	return $this->hasMany(OrderDetail::class);
    }
    public function OrderCancel()
    {
    	return $this->hasOne(OrderCancel::class);
    }
    public function OrderDelivery()
    {
    	return $this->hasOne(OrderDelivery::class);
    }
    public function OrderShipping()
    {
    	return $this->hasOne(OrderShipping::class);
    }
    public function OtherAddress()
    {
    	return $this->belongsTo(OtherAddress::class,"address_id");
    }
}
