<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    public function subcategories()
    {
    	return $this->hasMany(Subcategory::class);
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }

}
