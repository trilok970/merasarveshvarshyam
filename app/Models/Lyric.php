<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lyric extends Model
{
    use HasFactory;
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getFileAttribute($value) {
        return file_checker($value, 'lyric');
    }
    public function LyricCategory() {
        return $this->hasMany(LyricCategory::class);
    }
}
