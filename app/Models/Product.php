<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Product extends Model
{
    use HasFactory;
    protected $appends = ['is_favourite','unitname'];

    public function ProductImages()
    {
    	return $this->hasMany(ProductImage::class);
    }
    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
    public function subcategory()
    {
    	return $this->belongsTo(Subcategory::class);
    }
    public function subunits()
    {
    	return $this->belongsToMany(Subunit::class,'product_subunits','subunit_id','product_id');
    }
    public function ProductSubunit()
    {
    	return $this->hasMany(ProductSubunit::class);
    }
    public function getIsFavouriteAttribute()
    {
        $count = 0;
        $user = Auth::user();
        if($user) {
            $count = Favourite::where(['user_id'=>$user->id,'product_id'=>$this->id,'is_deleted'=>0])->count() ?? 0;
        }
        return $count;
    }
    public function getUnitnameAttribute()
    {
        return $unit = Unit::where(['id'=>$this->unit])->first()->name ?? "";
    }
    public function getImageAttribute($value) {
        return file_checker($value, 'avatar');
    }
    public function getThumbnailAttribute($value) {
        return file_checker($value, 'avatar');
    }
}
