<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subunit extends Model
{
    use HasFactory;
    public function unit() {
        return $this->belongsTo(Unit::class);
    }
    
    public function products()
    {
        return $this->belongsToMany(Product::class,'product_subunits','product_id','subunit_id');
    }
    
}
