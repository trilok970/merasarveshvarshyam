<?php
namespace App\Lib;
use App\Models\User;
use Auth;
use App\Models\Activity;
class ClientApi {
    public $live = false;

    public static function sendGcmNotify()   {        
        $fields = array(
                'priority' => "high",
                'registration_ids' => $reg_id,
                'data' => array("message" => $jsn_data, "content_available" => 1, "force-start" => 1, "sound" => "default"),
                'delay_while_idle' => false,
                'time_to_live' => $ttl,
                'collapse_key' => "" . $randomNum . ""
            );

        $headers = array(
            'Authorization: key=AAAAa-3E_Fg:APA91bHoXZ9RIqht_tHm32U1MAphfCgXcPp3ZjIEum4eYXwZOWHd91taFpNCzUqWjU58M29Yfe-SIb7Mt-DHFKE1n30l4b0EHbJUpukju9Syge1ySJjk-Rq-b4_KAKz96UfBJgak66VM',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
//            die('Problem occurred: ' . curl_error($ch));
        }

        curl_close($ch);
    }

}

?>