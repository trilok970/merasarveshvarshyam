<?php

namespace App\Lib;

class PushNotification {

    public $live = false;

    public static function sendIOSNotification($device_tokens, $message, $data = '') {

        $passphrase = 'kweeke123';
        $pem = base_path("public/push.pem");
        //$apns_url = 'gateway.push.apple.com';
        $apns_url = 'gateway.sandbox.push.apple.com';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        //stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'services/YachtMe_APN_Dis_Certificates.pem');

        $fp = stream_socket_client('ssl://' . $apns_url . ':2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
        $body['aps'] = array(
            'alert' => array(
                'title' => $message['title'],
                'body' => $message['description'],
            ),
            'data' => $data,
            'sound' => 'default',
            'badge' => 1
        );
        // Encode the payload as JSON
        $payload = json_encode($body);
        foreach ($device_tokens as $device_token) {
            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));
        }
        // Close the connection to the server
        fclose($fp);
    }

    public static function sendAndroidNotification($reg_id, $message, $data = '',$type="")   {
        $ttl = 86400;
        $randomNum = rand(10, 100);
        if (!is_array($reg_id)) {
            $reg_id =  array($reg_id);
        }
        $jsn_data = array("message" => $message,'data'=> $data);
        $fields = array(
            'priority' => "high",
            'registration_ids' => $reg_id,
            //'notification' => array("title"=> $message['title'],"body" => $message['description'],"click_action" => "FCM_PLUGIN_ACTIVITY"),
            'data' => array("title"=> $message['title'],"body" => $message['description'],'type' => $type,"data" => $data, "content_available" => 1, "force-start" => 1,"click_action" => "FCM_PLUGIN_ACTIVITY", "sound" => "default"),
            'delay_while_idle' => false,
            'time_to_live' => $ttl,
            'collapse_key' => "" . $randomNum . ""
        );

        $headers = array(
            'Authorization: key=AAAAFSMu8w4:APA91bGylHBUzw5FoPtwAx9POOcQFxIxMF6ZsRnMbr6aghH_MvNZ5vihNitssNBpp0ir1DaeTSCKp2Kj9d7XEqezwMoPPmHFe9HjMnDF--hO6icvCXx0hyGjswUrYMlDVXIifnH7erds',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        //print_r($result).'<br><br>';
        curl_close($ch);
    }

    public static function NotifyAll($users, $message=[], $data = "",$type="")
    {
        $users = is_array($users)?$users:explode(',',$users);
        $deviceinfo_android = \App\Models\UserDevices::join('users', 'users.id', '=', 'user_id')->whereIn('user_id',$users)->where('device_type','ANDROID')->where('notification_permissions',1)->pluck('device_id')->toArray();
        $deviceinfo_iso = \App\Models\UserDevices::join('users', 'users.id', '=', 'user_id')->whereIn('user_id',$users)->where('device_type','IOS')->where('notification_permissions',1)->pluck('device_id')->toArray();
        if(!empty($deviceinfo_android))
        {
            PushNotification::sendAndroidNotification($deviceinfo_android, $message, $data,$type);
        }
        if(!empty($deviceinfo_iso))
        {
            //PushNotification::sendIOSNotification($deviceinfo_iso, $message, $data,$type);
        }
    }
}

?>