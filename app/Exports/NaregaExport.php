<?php

namespace App\Exports;

use App\Models\Narega as ModelsNarega;
use App\Narega;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class NaregaExport implements FromArray
{
    /**
    * @return \Illuminate\Support\FromArray
    */
    public function array(): array
    {
        $users =  ModelsNarega::where(['is_deleted'=>0])->orderBy('id','desc')->get();
        $data[0][0] = 'Sr. No.';
        $data[0][1] = 'Name';
        $data[0][2] = 'Father Name';
        $data[0][3] = 'Email';
        $data[0][4] = 'Mobile No.';
        $data[0][5] = 'Dob';
        $data[0][6] = 'Post';
        $data[0][7] = 'Date Of Joining';
        $data[0][8] = 'Gram Panchayat';
        $data[0][9] = 'Block';
        $data[0][10] = 'District';
        $data[0][11] = 'State';
        $data[0][12] = 'Message';
        $j=1;
        foreach($users as $user)
        {
            $data[$j][0] = $j;
            $data[$j][1] = $user->name;
            $data[$j][2] = $user->father_name;
            $data[$j][3] = $user->email;
            $data[$j][4] = $user->mobile_no;
            $data[$j][5] = date("d-M-Y",strtotime($user->dob));
            $data[$j][6] = $user->post;
            $data[$j][7] = date("d-M-Y",strtotime($user->date_of_joining));
            $data[$j][8] = $user->gram_panchayat;
            $data[$j][9] = $user->block;
            $data[$j][10] = $user->district;
            $data[$j][11] = $user->state;
            $data[$j][12] = $user->message;

            $j++;
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        return $data;
    }
}
