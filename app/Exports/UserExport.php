<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
class UserExport implements FromArray
{
    /**
    * @return \Illuminate\Support\FromArray
    */
    public function array(): array
    {
        $users =  User::where(['is_deleted'=>0])->orderBy('id','desc')->get();
        $data[0][0] = 'Sr. No.';
        $data[0][1] = 'Name';
        $data[0][2] = 'County Code';
        $data[0][3] = 'Phone Number';
        $data[0][4] = 'Email';
        $data[0][5] = 'Profile Pic';
        $data[0][6] = 'Dob';
        $data[0][7] = 'Address';
        $data[0][7] = 'Type';
        $data[0][9] = 'Created On';
        $j=1;
        foreach($users as $user)
        {
            $data[$j][0] = $j;
            $data[$j][1] = $user->fullname;
            $data[$j][2] = $user->country_code;
            $data[$j][3] = $user->phone_number;
            $data[$j][4] = $user->email;
            $data[$j][5] = url($user->profile_pic);
            $data[$j][6] = date("d-M-Y",strtotime($user->dob));;
            $data[$j][7] = $user->address;
            $data[$j][8] = ucfirst($user->type);
            $data[$j][9] = date("d-M-Y",strtotime($user->created_at));

            $j++;
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        return $data;
    }
}
