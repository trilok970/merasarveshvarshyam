<?php

use Validations as CustomValidations;
use App\Models\Cms;
use App\Models\UserDevices;
use ImageResize as Image;
use App\Lib\VIDEOSTREAM;
use wapmorgan\Mp3Info\Mp3Info;

/*
 * function getRule to get validations rule by name
 * params string $name, bool $required
 */


if (!function_exists('getRule')) {

    function getRule($name, $required = false, $nullable = false) {
        return CustomValidations::getRule($name, $required, $nullable);
    }

}
if (!function_exists('show_date')) {

    function show_date($date) {
        if ($date) {
            return date(env('DATE_FORMAT_PHP', 'm/d/Y'), strtotime($date));
        }
    }

}
if (!function_exists('db_phone')) {

    function db_phone($phone) {
        if ($phone) {
            return str_replace([' ', '-'], '', $phone);
        } else {
            return $phone;
        }
    }

}

if (!function_exists('prep_url')) {

    function prep_url($str = '') {
        if ($str === 'http://' OR $str === '') {
            return '';
        }
        $url = parse_url($str);
        if (!$url OR ! isset($url['scheme'])) {
            return 'http://' . $str;
        }
        return $str;
    }

}

/* start by yogendra goyal */

if (!function_exists('all_file_upload')) {

    function all_file_upload($file, $path) {
        $extension = $file->getClientOriginalExtension();
        $orignalname = $file->getClientOriginalName();
        if (in_array($extension, ['WAV', 'wav', 'FLAC', 'flac', 'AIFF', 'aiff', 'WMA', 'wma', 'jpg', 'png', 'gif', 'JGP', 'PNG', 'GIF'])) {
            $fileNameExt = time() . "-" . rand(1000, 9999);
            $path = 'storage/app/public/albums/' . $path . '/' . $orignalname;
            move_uploaded_file($file, $path);
            return array(true, $orignalname, $extension, $orignalname);
        } else {
            return array(false, "You can upload the following format:  WAV, FLAC, AIFF, WMA.", '');
        }
    }

}

if (!function_exists('image_upload')) {

    function image_upload($file, $path) {
        // echo "check";exit;
        // $path = 'storage/app/public/store/' . $path . '/';
        // $path = 'public/' . $path . '/';
        $path =  $path . '/';
        $imgsize = getimagesize($file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $imgre = calculateDimensions($width, $height, 450, 450);
        $image = $file;
        $extension = $image->getClientOriginalExtension();
        $fileType = $image->getClientMimeType();
        $orignalname = $file->getClientOriginalName();
        $fileNameExt = time() . "-" . rand(1000, 9999);
        $fileName = $fileNameExt . '.' . $extension;               // renameing image
        $fileNamethum = $fileNameExt . '_thumb.' . $extension;
        if (in_array($extension, ['jpeg', 'jpg', 'JPG', 'JPEG', 'png', 'PNG', 'gif', 'GIF'])) {
            // if (!file_exists($path)) {
            //     mkdir($path, 666, true);
            // }
            $thumb_img = Image::make($image->getRealPath())->resize($imgre['width'], $imgre['height']);
            $thumb_img->save(public_path($path) . $fileNamethum, 100);
            $image->move(public_path($path), $fileName);
            return array(true, $path . $fileName, $path . $fileNamethum, $extension, $orignalname, $fileType);
        } else {
            return array(false, "file should be in jpeg, jpg,png,gif format / double extension not allow.", '');
        }
    }

}

if (!function_exists('video_upload')) {

    function video_upload($file, $path) {
        // $path = 'storage/app/public/' . $path . '/';
        $path =  'public/' .$path . '/';
        $extension = $file->getClientOriginalExtension();
        $orignalname = $file->getClientOriginalName();
        $fileNameExt = time() . "-" . rand(1000, 9999);
        $fileName = $fileNameExt . '.' . $extension;               // renameing image
        $fileNamethum = $fileNameExt . '_thumb.' . $extension;
        if (in_array($extension, ['mp4', 'MP4', 'mov', 'MOV', 'wmv', 'WMV', 'avi', 'AVI','mkv','MKV','flv','FLV','MPEG-2','mpeg-2'])) {
            $file->move(($path), $fileName);
            $fileNameExt = time() . "-" . rand(1000, 9999);
            $fileNamethum = makeThumbnailFromVideo(($path), $fileName, $fileNameExt);
            //$fileName = convertVideo($path, $fileName, $fileNameExt);
            $fileNamethum['duration'] = secondToMinutes($fileNamethum['duration']);
            return array(true, $path . $fileName, $path . $fileNamethum['thum'], $fileNamethum['duration'], $extension, $orignalname);
        } else {
            return array(false, "file should be in mp4 format / double extension not allow.", '');
        }
    }

}

if (!function_exists('makeThumbnailFromVideo')) {

    function makeThumbnailFromVideo($path, $tmp_file, $new_file) {
        $MOVIE = new VIDEOSTREAM();
        $tmpPath = $path . $tmp_file;
        $imageFile = $new_file . ".jpg";
        $detail = array();
        $MOVIE->load($tmpPath);
        $r = ($MOVIE->video_info());

        $s = $MOVIE->getDetailInfo();
        $size = $r['size'];
        $szArr = array("width" => $size['width'], "height" => $size['height']);
        $MOVIE->getFrameAtTime(3, 350, 400, 100, $path . basename($imageFile));
        return ['thum' => $imageFile, 'duration' => $r['duration']];
    }

}

if (!function_exists('convertVideo')) {

    function convertVideo($path, $tmp_file, $new_file) {
        $MOVIE = new VIDEOSTREAM();
        $tmpPath = $path . $tmp_file;
        $videoFile = $new_file . ".mp4";
        $detail = array();
        $MOVIE->load($tmpPath);
        $r = ($MOVIE->video_info());
        $s = $MOVIE->getDetailInfo();
        $size = $r['size'];
        $szArr = array("width" => $size['width'], "height" => $size['height']);
        $MOVIE->convertVideo($path . $videoFile);
        return ['video' => $videoFile, 'duration' => $r['duration']];
    }

}

if (!function_exists('file_checker')) {

    function file_checker($file, $filename = null) {

        /*if( \Request::segment(1)== 'api'){
            if (isset($file) && !empty($file) && file_exists(base_path() . '/' . $file)) {
                $images = ($file);
            } else {
                if ($filename == 'avatar') {
                    $images = ('public/img/avatar.jpg');
                } else if ($filename == 'cover') {
                    $images = ('public/img/cover.jpg');
                } else if ($filename == 'item') {
                    $images = ('public/img/default_profile_background.png');
                } else if ($filename == 'default_profile_background') {
                    $images = ('public/img/default_profile_background.png');
                } else {
                    $images = ('public/img/default.jpeg');
                }
            }
        }else{*/
        // echo ">>> ".$file;
        // echo "<br>".file_exists(base_path() . '/' . $file);
        // exit;

            if (isset($file) && !empty($file)) {
                $images = url($file);
            } else {
                if ($filename == '') {
                    $images = '';
                } else if ($filename == 'avatar') {
                    $images = url('assets/imges/avatar.jpg');
                } else if ($filename == 'cover') {
                    $images = url('public/img/cover.jpg');
                } else if ($filename == 'item') {
                    $images = url('public/img/default_profile_background.png');
                } else if ($filename == 'default_profile_background') {
                    $images = url('public/img/default_profile_background.png');
                }  else if ($filename == 'audio') {
                    $images = url('public/audio/audio.mp3');
                } else if ($filename == 'video') {
                    $images = url('public/video/video.mp4');
                } else if ($filename == 'lyric') {
        echo $filename;exit;

                    $images = url('public/lyric/lyric.lrc');
                } else {
                    $images = url('public/img/default.jpeg');
                }
            }
        //}
        return $images;
    }

}

if (!function_exists('file_checker_and_delete')) {

    function file_checker_and_delete($file) {
        if (isset($file) && !empty($file) && file_exists(base_path() . '/' . $file)) {
            unlink($file);
        }
    }

}

if (!function_exists('makeDirectory')) {

    function makeDirectory($path, $mode = 0777, $recursive = false, $force = false) {
        if ($force) {
            if (!file_exists($path)) {
                return @mkdir($path, $mode, $recursive);
            }
        } else {
            if (!file_exists($path)) {
                return mkdir($path, $mode, $recursive);
            }
        }
    }

}

if (!function_exists('makeDelete')) {

    function makeDelete($path) {
        if (file_exists($path)) {
            return rmdir($path);
        }
    }

}

if (!function_exists('renameDirectory')) {

    function renameDirectory($oldpath, $newpath) {
        rename($oldpath, $newpath);
    }

}

function createStatus($status, $id, $array = null, $class = 'changeStatus', $path = null) {
    if (!$array) {
        $array = ['1' => 'Approved', '0' => 'Not Approved'];
    }
    return $html = Form::select('active', $array, $status, ['class' => 'form-control input-border-bottom   ' . $class, 'style' => 'width:75px', 'id' => $id, 'data-path' => routeUser($path)]);
}

function createVerify($status, $id, $array = null, $class = 'changeVerify', $path = null) {
    if (!$array) {
        $array = ['1' => 'Verify', '0' => 'Not-verify'];
    }
    return $html = Form::select('confirmed', $array, $status, ['class' => 'form-control ' . $class, 'id' => $id, 'data-path' => $path]);
}

function createEditAction($route, $para, $type = null) {
    if ($type == 'ajax') {
        $html = '<a href="javascript:;" title="' . __('admin_lang.edit') . '"  class="btn btn-xs btn-primary edit-button" data-edit_id ="' . $para['id'] . '"><i class="fas fa-edit"></i></a>';
    } else {
        $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.edit') . '"  class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></a>';
    }
    return $html;
}

function createChargeAction($id, $sub_exp_date) {
    $btn = "btn-success";
    $title = "Subscription expire on " . $sub_exp_date;
    if (strtotime($sub_exp_date) < strtotime(date('Y-m-d'))) {
        $btn = "btn-danger";
        $title = "Subscription expired on " . $sub_exp_date;
    }
    $html = '<a title="' . $title . '" href="javascript:void(0)" class="btn btn-xs ' . $btn . ' chargeamt" id="' . $id . '">';
    $html .= '<i class="fas fa-usd" aria-hidden="true"></i>';
    $html .= '</a>';
    return $html;
}

function createViewAction($route, $para) {
    $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.view') . '" class="btn btn-xs btn-warning"><i class="fas fa-eye"></i></a>';
    return $html;
}

function createIntrestedAction($route, $para) {
    $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.interested') . '" class="btn btn-xs btn-default"><i class="fas fa-ticket "></i></a>';
    return $html;
}

function createDefaultAction($route, $para, $class, $icon, $title) {
    $html = '<a href="' . routeUser($route, $para) . '" data-toggle="tooltip" data-placement="top" title="' . __('admin_lang.' . $title) . '" class="btn btn-xs btn-' . $class . '"><i class="fas fa-' . $icon . ' "></i></a>';
    return $html;
}

function createDeleteAction($route, $para, $base_route, $role = null) {
    $html = '<a href="javascript:void(0)" class="btn btn-xs btn-danger del" title="' . __('admin_lang.delete') . '" data-route="' . routeUser($route, $para) . '" data-base_url="' . routeUser($base_route, $role) . '">';
    $html .= '<i class="fas fa-trash"></i>';
    $html .= '</a>';
    return $html;
}

function createFloorAction($route, $para, $base_route, $role = null) {
    $html = '<a href="javascript:void(0)" class="btn btn-xs btn-primary add_floor" title="' . __('admin_lang.add_floor') . '" data-route="' . routeUser($route, $para) . '" data-base_url="' . routeUser($base_route, $role) . '">';
    $html .= __('admin_lang.add_floor');
    $html .= '</a>';
    return $html;
}

function checkRolePermission($key, $screen = 0) {
    if(Auth::user()->role_id==1)
       return true;
    $role_id = Auth::user()->role_id;
    $q = \App\Models\Permission::where('role_id', $role_id)->whereHas('getUserPermission', function($q)use($key) {
        $q->where('name', $key);
    });
    $data = $q->count();
    if ($data > 0) {
        return true;
    } else {
        if ($screen == 0) {
            echo view('errors.permission_denied');
            exit;
        } else {
            return false;
        }
    }
}

function checkPermission($user_id, $permission_id) {
    $data = PermissionRole::where('user_id', $user_id)->where('permission_id', $permission_id)->first();
    if (isset($data)) {
        if ($data->permission == 1) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

if (!function_exists('setActivity')) {

    function setActivity($type, $type_id, $user_id, $title, $data, $slient = null, $other_id = 0) {
        //$result = PushNotification::Notify($user_id, $data, $type, $type_id, array(), $title,$slient);
        $message = $data;
        $data = new Activity;
        $data->notify_from = Auth::user()->id;
        $data->user_id = implode(',', $user_id);
        $data->table_name = $type;
        $data->table_column_id = $type_id;
        $data->other_id = isset($message['value']) ? $message['value'] : 0;
        $data->title = $message['title'];
        $data->description = $message['description'];
        $data->is_read = '0';
        $data->start_date = $message['start_date'];
        $data->end_date = $message['end_date'];
        $data->save();
        return 'true';
    }

}

/* yogendra */

function createSelfDeleteAction($id) {
    $html = '<a href="javascript:void(0)" class="btn btn-xs btn-danger del_dis" id="' . encrypt($id) . '">';
    $html .= '<i class="fa fa-trash"></i>';
    $html .= '</a>';
    return $html;
}

function createEnDeleteAction($id, $school_id = 0) {
    $html = '<a href="javascript:void(0)" class="btn btn-xs btn-danger del_dis" id="' . $id . '" data-school="' . $school_id . '">';
    $html .= '<i class="fa fa-trash"></i>';
    $html .= '</a>';
    return $html;
}

function createLink($url, $val, $target = '') {
    $html = '<a href="' . $url . '" class="custom_link"> ' . $val . '</a>';
    if ($target != '') {
        $html = '<a href="' . $url . '" class="custom_link"> ' . $val . '</a>';
    } else {
        $html = '<a target="_blank" href="' . $url . '" class="custom_link"> ' . $val . '</a>';
    }

    return $html;
}

function pr($arr, $isStop = 0) {
    echo "<pre>";
    print_r($arr);
    echo "<pre>";
    if ($isStop)
        die("stopping");
}

function YMDDateFormat($date) {
    return date('Y-m-d h:m:A', strtotime($date));
}

function dateFormat($date){
    return date('M d, Y', strtotime($date));
}

function imageError() {
    return "Only these formats are allowed : jpeg,jpg,png,gif,bmp,ico.";
}

function uniqueRandomString($name) {
    $string = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 4);
    return strtoupper($name . $string);
}

function userUrl($url) {
    if (Auth::user()->role_id == '2') {
        return url('subadmin/' . $url);
    }
    if (Auth::user()->role_id == '3') {
        return url('subadmin/' . $url);
    } else if (Auth::user()->role_id == '5') {
        return url('internship/' . $url);
    } else {
        return url('admin/' . $url);
    }
}

function redirectUser($url) {
    if (Auth::user()->role_id == '2') {
        return redirect("subadmin/" . $url);
    } else if (Auth::user()->role_id == '3') {
        return redirect("subadmin/" . $url);
    } else if (Auth::user()->role_id == '5') {
        return redirect("internship/" . $url);
    } else {
        return redirect("admin/" . $url);
    }
}

function redirectRoute($name, $pream = null) {
    if (Auth::user()->role_id == '2') {
        if (isset($pream)) {
            return redirect()->route('subadmin.' . $name, $pream);
        } else {
            return redirect()->route('subadmin.' . $name);
        }
    } else if (Auth::user()->role_id == '3') {
        if (isset($pream)) {
            return redirect()->route('subadmin.' . $name, $pream);
        } else {
            return redirect()->route('subadmin.' . $name);
        }
    } else if (Auth::user()->role_id == '5') {
        if (isset($pream)) {
            return redirect()->route('internship.' . $name, $pream);
        } else {
            return redirect()->route('internship.' . $name);
        }
    } else {
        if (isset($pream)) {
            return redirect()->route('admin.' . $name, $pream);
        } else {
            return redirect()->route('admin.' . $name);
        }
    }
}

function routeUser($name, $pream = null) {
    if (Auth::user()->role_id == '2') {
        if (isset($pream)) {
            return route('subadmin.' . $name, $pream);
        } else {
            return route('subadmin.' . $name);
        }
    } else if (Auth::user()->role_id == '3') {
        if (isset($pream)) {
            return route('subadmin.' . $name, $pream);
        } else {
            return route('subadmin.' . $name);
        }
    } else if (Auth::user()->role_id == '5') {
        if (isset($pream)) {
            return route('internship.' . $name, $pream);
        } else {
            return route('internship.' . $name);
        }
    } else {
        if (isset($pream)) {
            return route('admin.' . $name, $pream);
        } else {
            return route('admin.' . $name);
        }
    }
}

function routeFormUser($name) {
    if (Auth::user()->role_id == '2') {
        return "subadmin." . $name;
    } else if (Auth::user()->role_id == '3') {
        return "subadmin." . $name;
    } else if (Auth::user()->role_id == '5') {
        return "subadmin." . $name;
    } else {
        return "admin." . $name;
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function calculateDimensions($width, $height, $maxwidth, $maxheight) {

    if ($width != $height) {
        if ($width > $height) {
            $t_width = $maxwidth;
            $t_height = (($t_width * $height) / $width);
            //fix height
            if ($t_height > $maxheight) {
                $t_height = $maxheight;
                $t_width = (($width * $t_height) / $height);
            }
        } else {
            $t_height = $maxheight;
            $t_width = (($width * $t_height) / $height);
            //fix width
            if ($t_width > $maxwidth) {
                $t_width = $maxwidth;
                $t_height = (($t_width * $height) / $width);
            }
        }
    } else
        $t_width = $t_height = min($maxheight, $maxwidth);

    return array('height' => (int) $t_height, 'width' => (int) $t_width);
}

function getSession() {
    $current_date = date('Y');
    $year_array = [];
    $date = '';
    $date_next = '';

    for ($i = 1; $i <= 10; $i++) {
        $b = $current_date + 1;
        $a = $current_date . '-' . substr($b, 2, 2);
        $year_array[$a] = $a;
        $current_date++;
    }
    return $year_array;
}

function secondToMinutes($seconds) {
    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds-($hours*3600)) / 60);
    //$minutes = floor($seconds / 60);
    //$houes = floor($minutes / 60);
    $secondsleft = $seconds % 60;
    if ($minutes < 10)
        $minutes = "0" . $minutes;
    if ($hours < 10)
        $hours = "0" . $hours;
    if ($secondsleft < 10)
        $secondsleft = "0" . $secondsleft;
    return $hours. ":" .$minutes . ":" . $secondsleft;
}

function sendOtp($phone, $country_code, $type = '', $password = '') {
    if (isset($phone) && isset($country_code)) {
        $OTP_ACCOUNT_NO = "11032402";
        $OTP_USERNAME = "sjs.vercode";
        $OTP_PASSWORD = "aas55694914";
        $phonecode = str_replace('+', '', $country_code);
        $mob = $phonecode . $phone; // Mobile Number
        $curl = curl_init();

        $msg = smsTemplate($type, $password);
//         echo "https://api.accessyou.com/sms/sendsms-vercode.php?msg=".$msg."&phone=".$mob."&user=".$OTP_USERNAME."&pwd=".$OTP_PASSWORD."&accountno=".$OTP_ACCOUNT_NO;die;
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.accessyou.com/sms/sendsms-vercode.php?msg=" . urlencode($msg)
            . "&phone=" . $mob . "&user="
            . $OTP_USERNAME . "&pwd=" . $OTP_PASSWORD . "&accountno=" . $OTP_ACCOUNT_NO,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
//      echo $response;die();
    }
}

function getAttrs($attrTypeId, $attributesjson) {
    $attrs = Attribute::where('status', 1)->where('attribute_type_id', $attrTypeId)->get();
    $html = '';
    $i = 0;

    foreach ($attrs as $attrData) {

        $html .= '<input type="hidden" name="Attribute[' . $attrTypeId . '][attid][att_id][]" id="attid" value="' . $attrData->id . '">';
        $html .= '<input type="hidden" name="Attribute[' . $attrTypeId . '][attrlabel][attr_label][]" id="attrlabel" value="' . $attrData->title . '">';
        $html .= '<tr>';
        $html .= '<td>' . ucfirst($attrData->title) . '</td>';
        $html .= '<td><input type = "text" name = "Attribute[' . $attrTypeId . '][attrprice][attr_price][]" id = "size_price" value=""></td>';
        $html .= '</tr>';
        $i++;
    }
    echo $html;
}
// if (!function_exists('image_upload')) {
//     function image_upload($file,$filePath,$fileType) {
//         if($file) {
//             $diskName = env('DISK');
//             $extension = $file->extension();
//             $fileName  = $filePath.'/' . time() . ".$extension";
//             if($diskName == 'local') {

//                 $ff = $file->move(public_path($filePath), $fileName);

//             }
//             else if($diskName == 's3') {
//                 $filePath = $fileName;
//                 $disk = \Storage::disk('s3');
// 				$disk->put($filePath, fopen($file, 'r+'), $fileType);
//             }
//         }

//         return $fileName ?? "";
//         }
//     }
    if (!function_exists('audio_upload')) {
        function audio_upload($file,$filePath,$fileType) {
            if($file) {
                $diskName = env('DISK');
                $extension = $file->extension();
                $fileName  = $filePath.'/' . time() . ".$extension";
                if($diskName == 'local') {

                    $ff = $file->move(public_path($filePath), $fileName);
                    $duration = get_audio_duration($ff);
                }
                else if($diskName == 's3') {
                    $disk = \Storage::disk('s3');
                    $disk->put($fileName, fopen($file, 'r+'), $fileType);

                    $ff = $file->move(public_path($filePath), $fileName);
                    $duration = get_audio_duration($ff);
                    file_delete($ff);
                }
            }
            $array = array("fileName"=>$fileName ?? "",'finalFilePath'=>$finalFilePath ?? "","extension"=>$extension ?? "","duration"=>$duration ?? "");
            return $array;
            }
        }

 if (!function_exists('file_delete')) {
        function file_delete($file) {
            if(\File::exists($file)) {
                \File::delete($file);
            }
        }
    }
    if (!function_exists('get_audio_duration')) {
        function get_audio_duration($file) {
            if($file) {
                $audio1 = new Mp3Info($file, true);
                return $audio1->duration ?? 0;
            }
        }
    }
    if (!function_exists('image_upload_social')) {
        function image_upload_social($fileName,$filePath,$imgurl) {
            if($fileName) {
                $diskName = env('DISK');
                $ppath = public_path().'/'.$filePath.'/';

                if($diskName == 'local') {
                    file_put_contents($ppath.$fileName,file_get_contents($imgurl));
                }
                else if($diskName == 's3') {

	               file_put_contents($ppath.$fileName,file_get_contents($imgurl));
                $destinationPath = '/images/';
                $ppath = public_path().'/'.$destinationPath;
                $file = ($ppath.$fileName);
                $filePath = $filePath.'/'.$fileName;

                $disk = \Storage::disk('s3');
                $disk->put($filePath, fopen($file, 'r+'), 'public');
                file_delete($file);
                }
            }

            return $fileName ?? "";
            }
        }
        if (!function_exists('lyric_upload')) {
                function lyric_upload($file,$filePath,$fileType) {
                    if($file) {
                        $diskName = env('DISK');
                        $extension = $file->getClientOriginalExtension();
                        $fileName  = $filePath.'/' . time(). "-" . rand(1000, 9999) . ".$extension";
                        if (in_array($extension, ['lrc', 'LRC'])) {

                            if($diskName == 'local') {
                            $ff = $file->move(public_path($filePath), $fileName);
                            }
                            else if($diskName == 's3') {
                            $disk = \Storage::disk('s3');
                            $disk->put($fileName, fopen($file, 'r+'), $fileType);
                            }
                            return array(true, $fileName, $extension);
                    }
                    else {
                        return array(false, "You can upload the following format:  lrc, LRC.", '');
                    }
                    }
                }
            }
            if (!function_exists('image_upload_social')) {
                function image_upload_social($fileName,$filePath,$imgurl) {
                    if($fileName) {
                        $diskName = env('DISK');
                        $ppath = public_path().'/'.$filePath.'/';

                        if($diskName == 'local') {
                            file_put_contents($ppath.$fileName,file_get_contents($imgurl));
                        }
                        else if($diskName == 's3') {

                           file_put_contents($ppath.$fileName,file_get_contents($imgurl));
                        $destinationPath = '/images/';
                        $ppath = public_path().'/'.$destinationPath;
                        $file = ($ppath.$fileName);
                        $filePath = $filePath.'/'.$fileName;

                        $disk = \Storage::disk('s3');
                        $disk->put($filePath, fopen($file, 'r+'), 'public');
                        file_delete($file);
                        }
                    }

                    return $fileName ?? "";
                }
            }
