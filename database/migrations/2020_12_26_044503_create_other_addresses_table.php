<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('other_addresses'))
        {
        Schema::create('other_addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->string('phone_number');
            $table->string('address_type',100);
            $table->text('location');
            $table->string('street');
            $table->string('apartment');
            $table->float('lat');
            $table->float('lng');
            $table->string('pincode',100)->default(0);
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_addresses');
    }
}
