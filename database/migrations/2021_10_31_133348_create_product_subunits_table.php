<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSubunitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_subunits'))
        {
        Schema::create('product_subunits', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('subunit_id');
            $table->bigInteger('product_id');
            $table->integer('price')->default(0);
            $table->integer('mrp')->default(0);
            $table->integer('qty')->default(0);
            $table->integer('min_qty')->default(0);
            $table->integer('max_qty')->default(0);
            $table->bigInteger('color_id')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_subunits');
    }
}
