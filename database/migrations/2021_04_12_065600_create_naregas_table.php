<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNaregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('naregas'))
        {
        Schema::create('naregas', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('father_name');
            $table->string('email')->nullable();
            $table->string('mobile_no');
            $table->date('dob');
            $table->string('post');
            $table->date('date_of_joining');
            $table->string('gram_panchayat')->nullable();
            $table->string('block');
            $table->string('district');
            $table->string('state');
            $table->string('donation');
            $table->text('message');
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('naregas');
    }
}
