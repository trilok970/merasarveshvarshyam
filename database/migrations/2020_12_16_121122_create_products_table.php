<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('products'))
        {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->default(0);
            $table->integer('subcategory_id')->default(0);
            $table->string('name');
            $table->integer('qty')->default(0);
            $table->string('code');
            $table->integer('min_qty')->default(0);
            $table->integer('max_qty')->default(0);
            $table->float('price')->default(0);
            $table->float('mrp')->default(0);
            $table->integer('unit')->default(0);
            $table->string('weight');
            $table->text('description');
            $table->string('image')->default('default.jpg');
            $table->string('thumbnail')->default('default.jpg');
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
