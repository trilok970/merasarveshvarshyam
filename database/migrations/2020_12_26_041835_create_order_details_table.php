<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('order_details'))
        {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->integer('order_id')->default(0);
            $table->string('order_no')->default(0);
            $table->date('order_date');
            $table->integer('product_id')->default(0);
            $table->string('product_name');
            $table->integer('qty')->default(0);
            $table->float('price')->default(0);
            $table->float('total_price')->default(0);
            $table->string('payment_mode');
            $table->enum('status',['complete','failed'])->default('failed');
            $table->string('image')->default('uploads/images/default.jpg');
            $table->string('thumbnail')->default('uploads/thumbnail/default.jpg');
            $table->string('type');
            $table->integer('gift_id')->default(0);
            $table->text('description');
            $table->integer('tax')->default(0);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
