const Common = function() {
    this.__construct = function() {
        this.addForm();
    };

    this.addForm = function() {
       $('.add-form').submit(function(event) {
            event.preventDefault();
            const url = $(this).attr('action');
            const postData = $(this).serialize();
            const rediretURL = $(this).data('url');
            $("div > .error").remove();
            $.post(url,postData,function(out) {
                if(out.status === 0) {
                    console.log(out.status);
                   for(let i in out.errors) {
                        $('#'+i).parent('div').append('<span class="error">'+out.errors[i]+'</span>');
                   }
                } else if(out.status === 1) {
                    console.log(out.status);

                    $('#error-msg').removeClass('d-none alert-danger').addClass('alert-success')
                        .html('<i class="fa fa-check"></i> <strong>Success:</strong> <span>'+out.msg+'</span>');

                    setTimeout(function() {
                        window.location = rediretURL;
                    },1000);

                } else if(out.status === -1) {
                    console.log(out.status);

                    $('#error-msg').removeClass('d-none alert-success').addClass('alert-danger')
                        .html('<div class="fa exclamation-triangle"></i> <strong>Error:</strong> <span>'+out.msg+'</span>');
                }
            });
       });
    };

    this.__construct();
};

const common = new Common();
