const Common = function() {
    this.__construct = function() {
        this.addForm();
    };

    this.addForm = function() {
       $('.add-form').submit(function(event) {
            event.preventDefault();
            const url = $(this).attr('action');
            // const postData = $(this).serialize();
            const postData = new FormData(this);
            const rediretURL = $(this).data('url');
            $(".form-group > .error").remove();
            $("div> .error").remove();
            $("label > .error").remove();
            $(".error_div").hide();

            var self = this;

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:url,
                type:'post',
                data:postData,
                processData: false,
                contentType: false,
                success:function(out){
                    if(out.status === 0) {
                        console.log(out.status);
                        $(".error_div").show();
                       for(let i in out.errors) {
                            $('#'+i).parent('div').append('<span class="error">'+out.errors[i]+'</span>');
                       }
                       for(var i in out.errors) {
                        console.log(i);
                        let index = i.replace(/^\D+/g,'');
                        let feild = i.replace(/\.[0-9]/g,'').trim();
                        // console.log(feild);
                        console.log(index);
                        let error = out.errors[i].toString();
                        error = error.replace(/\.(\d)+/g,'');
                        error = error.replace(/_/g,' ');
                        $(self).eq(index).find('.'+feild).parents("label").append('<span class="error">'+error+'</span>');

                        // let index = i.replace(/^\D+/g,'');
                        // let feild = i.replace(/\.[0-9]/g,'').trim();
                        // console.log("feild >> "+feild);
                        // let error = out.errors[i].toString();
                        //     error = error.replace(/\.(\d)+/g,'');
                        //     error = error.replace(/_/g,' ');
                        // $(".model_string").find(".create-diff-inverter-form").eq(index).find('.'+feild).parents(".form-group").append('<span class="error">'+error+'</span>');
                    }
                    } else if(out.status === 1) {
                        console.log(out.status);
    
                        $('#error-msg').removeClass('d-none alert-danger').addClass('alert-success')
                            .html('<i class="fa fa-check"></i> <strong>Success:</strong> <span>'+out.msg+'</span>');
    
                        setTimeout(function() {
                            window.location = rediretURL;
                        },1000);
    
                    } else if(out.status === -1) {
                        console.log(out.status);
    
                        $('#error-msg').removeClass('d-none alert-success').addClass('alert-danger')
                            .html('<div class="fa exclamation-triangle"></i> <strong>Error:</strong> <span>'+out.msg+'</span>');
                    }
                }

            });
            // $.post(url,postData,function(out) {
            //     if(out.status === 0) {
            //         console.log(out.status);
            //        for(let i in out.errors) {
            //             $('#'+i).parents('.form-group').append('<span class="error">'+out.errors[i]+'</span>');
            //        }
            //     } else if(out.status === 1) {
            //         console.log(out.status);

            //         $('#error-msg').removeClass('d-none alert-danger').addClass('alert-success')
            //             .html('<i class="fa fa-check"></i> <strong>Success:</strong> <span>'+out.msg+'</span>');

            //         setTimeout(function() {
            //             window.location = rediretURL;
            //         },1000);

            //     } else if(out.status === -1) {
            //         console.log(out.status);

            //         $('#error-msg').removeClass('d-none alert-success').addClass('alert-danger')
            //             .html('<div class="fa exclamation-triangle"></i> <strong>Error:</strong> <span>'+out.msg+'</span>');
            //     }
            // });
       });
    };

    this.__construct();
};

const common = new Common();
