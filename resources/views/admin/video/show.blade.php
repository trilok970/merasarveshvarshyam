@extends('layouts.admin')
@section('title','Video Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Video</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/video')}}">Video</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">Video Details</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/video')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Videos" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Videos
                                        </a>
                                    </div>
                                </div>

                                <div class="table-responsive">


                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <td><b>{{$video->title}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Category</th>
                                                <td>
                                                @foreach($video->videoCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}"><b>{{$value->music_category->name}}</b></span>
                                                @endforeach

                                                </td>

                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td><b>{{$video->description}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Image</th>
                                                <td><a target="_blank" href="{{$video->image}}"><img src="{{$video->thumbnail}}" width="50" /></a></td>
                                            </tr>
                                            <tr>
                                                <th>File</th>
                                                <td>
                                                @if($video->type == 'File')
                                                <video width="600" controls>
                                                <source src="{{ $video->file }}" type="video/{{$video->extension}}">
                                                Your browser does not support the video element.
                                                </video>
                                                @else
                                                {{-- <div id="ytplayer"></div> --}}
                                                <iframe width="420" height="315"
                                                src="{{ $video->youtube_url }}">
                                                </iframe>
                                                @endif
                                            </td>
                                            </tr>
                                            {{-- https://www.youtube.com/embed/tgbNymZ7vqY --}}

                                        </thead>



                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            @push('scripts')

            {{-- <script>
                // Load the IFrame Player API code asynchronously.
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/player_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                // Replace the 'ytplayer' element with an <iframe> and
                // YouTube player after the API code downloads.
                var player;
                function onYouTubePlayerAPIReady() {
                  player = new YT.Player('ytplayer', {
                    height: '360',
                    width: '640',
                    videoId: 'jbwhv-XhSBg'
                  });
                }
              </script> --}}
              @endpush

@endsection
