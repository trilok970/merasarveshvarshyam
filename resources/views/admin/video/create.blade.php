@extends('layouts.admin')
@section('title','Video Create')
@section('content')
@push('stylesheets')
<link rel="stylesheet" href="{{url('dist/css/selectize.css')}}" />
<link rel="stylesheet" href="{{url('dist/css/selectize.min.css')}}" />

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Video</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/video')}}">Video</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url(SEGMENT.'/video')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/video')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Video" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Video
                                        </a>
                                    </div>
                            </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{old('title')}}">
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Category</label>
                                    <div class="col-md-6">
                                        <select type="text" class="selectize-multiple" id="music_category_id" name="music_category_id[]"  multiple data-live-search="true">
                                        @foreach($categories as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('music_category_id')}}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Description</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" id="description" name="description" placeholder="Enter Description" >{{old('description')}}</textarea>
                                        <span class="text-danger">{{$errors->first('description')}}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Image</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control image" id="image" name="image" inc_val="1" placeholder="Choose image" value="{{old('image')}}">
                                        <span class="text-danger">{{$errors->first('image')}}</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <img style="display: none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Type</label>
                                    <div class="col-md-6">
                                        <select  class="form-control" id="type" name="type" >
                                            <option value="File"  {{ old('type') == 'File' ? "selected":"" }}>File</option>
                                            <option value="Youtube" {{ old('type') == 'Youtube' ? "selected":"" }}>Youtube</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('type')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row file-div">
                                    <label class="col-md-2 m-t-15">Video File</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="file" name="file" placeholder="Choose image" value="{{old('image')}}">
                                        <span class="text-danger">{{$errors->first('file')}}</span>
                                    </div>
                                    <div class="col-lg-4">

                                    </div>
                                </div>
                                <div class="form-group row youtube-div" style="display: none;">
                                    <label class="col-md-2 m-t-15">Youtube Url</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="youtube_url" name="youtube_url" placeholder="Enter Youtube Url" value="{{old('youtube_url')}}">
                                        <span class="text-danger">{{$errors->first('youtube_url')}}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/video')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        @push('scripts')
        <script src="{{url('dist/js/selectize.min.js')}}"></script>
        <script src="{{url('dist/js/form-selectize.min.js')}}"></script>

          <script>
        function show_file(type) {
            if(type == 'File') {
                    $(".youtube-div").hide();
                    $(".file-div").show();
                }
                else {
                    $(".youtube-div").show();
                    $(".file-div").hide();
                }
        }
        $(document).ready(function(){
            var type = $('#type').val();
            show_file(type);
            /* validate */
            $('#select-beast').selectize({
                create: true,
                sortField: 'text'
            });
            $("#type").change(function() {
                var type = $(this).val();
                show_file(type);
            });
            function readURL1(input)
            {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);

                $('#image_preview').hide();
                $('#image_preview').fadeIn(650);
                }
            reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function() {
            readURL1(this);
            });

        });



    </script>
    @endpush
@endsection
