@extends('layouts.admin')
@section('title','Change Password')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Change Password</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/change-password')}}">Change Password</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">
                    @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
                @if(session('error_message'))
                <p class="alert alert-danger">{{session('error_message')}}</p>
            @endif

                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url(SEGMENT.'/change-password')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/edit-profile')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All edit-profile" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            {{-- All edit-profile --}}
                                        </a>
                                    </div>
                            </div>

                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Current Password</label>
                                    <div class="col-md-10">
                                        <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Current Password" value="">
                                        <span class="text-danger">{{$errors->first('current_password')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">New Password</label>
                                    <div class="col-md-10">
                                        <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" value="">
                                        <span class="text-danger">{{$errors->first('new_password')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Password Confirmation</label>
                                    <div class="col-md-10">
                                        <input type="password" class="form-control" id="new_password" name="password_confirmation" placeholder="Password Confirmation" value="">
                                        <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                                    </div>
                                </div>







                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/dashbaord')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->


          <script>
        $(document).ready(function(){

            /* validate */



        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                name: {
                    required: true
                },
                image: {
                    required: true
                },

                description: {
                    required: true
                },


            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });


     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });



        });






    </script>
@endsection
