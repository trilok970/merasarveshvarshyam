@extends('layouts.admin')
@section('title','My Profile')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">My Profile</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/my-profile')}}">My Profile</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">show</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">Admin Details</h5></label>
                                    <div class="col-md-10">
                                         {{-- <a href="{{url(SEGMENT.'/user')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Users" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Users
                                        </a> --}}
                                    </div>
                                </div>
                                <div class="table-responsive">
                                   <table class="table table-striped table-bordered">
                               <tbody>

                                    <tr>
                                        <th style="width:30%">name</th>
                                        <td style="width:70%">{{$user->name}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Email</th>
                                        <td style="width:70%">{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%">Phone Number</th>
                                        <td style="width:70%"> {{ $user->phone_number }}</td>
                                    </tr>

                                    <tr>
                                        <th style="width:30%">Profile Pic</th>
                                        <td style="width:70%">
                                            <div class="avatar-xxl">
                                                <a target="_blank" href="{{ $user->profile_pic }}" >
                                                <img width="70" src="{{$user->profile_pic}}" class="avatar-img">
                                                </a>
                                            </div>
                                        </td>

                                </tbody>
                            </table>
                                </div>

                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
