@extends('layouts.admin')
@section('title','Dashboard')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashbaord</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
                <div class="row">

                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a class="text-white" href="{{ route('user.index') }}">
                        <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                <h6 class="text-white" data-toggle='tooltip' title="Users">Users ({{ $data['user_count'] }})  </h6>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a class="text-white" href="{{ route('product.index') }}">
                        <div class="card card-hover">
                            <div class="box bg-danger text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                <h6 class="text-white" data-toggle='tooltip' title="Products">Products ({{ $data['product_count'] }})  </h6>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a class="text-white" href="{{ route('category.index') }}">
                        <div class="card card-hover">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                <h6 class="text-white" data-toggle='tooltip' title="Categories">Categories ({{ $data['category_count'] }})  </h6>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a class="text-white" href="{{ route('subcategory.index') }}">
                        <div class="card card-hover">
                            <div class="box bg-warning text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                <h6 class="text-white" data-toggle='tooltip' title="Subcategories">Subcategories ({{ $data['subcategory_count'] }}) </h6>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a class="text-white" href="{{ route('orders.index') }}">
                        <div class="card card-hover">
                            <div class="box bg-primary text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                <h6 class="text-white" data-toggle='tooltip' title="Orders">Orders ({{ $data['order_count'] }})  </h6>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a class="text-white" href="{{ route('musiccategory.index') }}">
                        <div class="card card-hover">
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-account-circle"></i></h1>
                                <h6 class="text-white" data-toggle='tooltip' title="Music Categories">Music Categories ({{ $data['musiccategory_count'] }})  </h6>
                            </div>
                        </div>
                        </a>
                    </div>





                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
