 <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/user')}}" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu">Users</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/banner')}}" aria-expanded="false"><i class="mdi mdi-folder-multiple-image"></i><span class="hide-menu">Banner</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fab fa-product-hunt"></i><span class="hide-menu">Products </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/category')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Category </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/subcategory')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Sub Category </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/color')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Color </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/unit')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Unit </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/subunit')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Subunit </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/product')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Product </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/promocode')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Promo Code </span></a></li>


                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-music"></i><span class="hide-menu">Music Section </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/musiccategory')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Music Category </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/audio')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Audios</span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/video')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Videos </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/lyric')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Lyrics </span></a></li>


                            </ul>
                        </li>
                        {{-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/category')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Category</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/subcategory')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Subcategory</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/product')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Product</span></a></li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url(SEGMENT.'/promocode')}}" aria-expanded="false"><i class="mdi mdi-library-plus"></i><span class="hide-menu">Promo code</span></a></li> --}}

                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fab fa-first-order"></i><span class="hide-menu">Orders </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> All </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders/pending')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Pending </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders/received')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Received </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders/shipped')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Shipped </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders/delivered')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Delivered </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders/canceled')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Canceled </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/orders/failed')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Failed </span></a></li>

                            </ul>
                        </li>



                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Settings </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/cms')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> CMS Pages </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/content')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Content </span></a></li>
                                <li class="sidebar-item"><a href="{{url(SEGMENT.'/deliverycharge')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Delivery Charges </span></a></li>

                            </ul>
                        </li>



                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
