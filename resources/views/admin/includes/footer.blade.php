  <footer class="footer text-center">
                All Rights Reserved by {{ env('APP_NAME') }} Admin.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <input type="hidden" id="img_url" value="{{url('images/loader.gif')}}" />
        <input type="hidden" id="update_status_url" value="{{url('update-status')}}" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{url('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{url('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{url('assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{url('dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{url('dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{url('dist/js/custom.min.js')}}"></script>

    <script src="{{url('assets/libs/flot/excanvas.js')}}"></script>
    <script src="{{url('assets/libs/flot/jquery.flot.js')}}"></script>
    <script src="{{url('assets/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{url('assets/libs/flot/jquery.flot.time.js')}}"></script>
    <script src="{{url('assets/libs/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{url('assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
    <script src="{{url('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{url('dist/js/pages/chart/chart-page-init.js')}}"></script>
    <script src="{{url('assets/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{url('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{url('assets/libs/bootbox/bootbox.min.js')}}"></script>
    <script src="{{url('dist/js/loadingoverlay.min.js')}}"></script>
    <script>
        $(document)
            .ajaxStart(function () {
                // $('#AjaxLoader').show();
                $("body").LoadingOverlay("show");
            })
            .ajaxStop(function () {
                $("body").LoadingOverlay("hide");
                // $('#AjaxLoader').hide();
            });
    
            $.ajaxSetup({
                    headers: {
                        'Access-Control-Allow-Headers': '*',
                        'Accept': 'xml'
                    }
                });
    </script>
   <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/

         $(function() {

    $(".alert-success").fadeOut(5000);
    $(".alert-danger").fadeOut(5000);
});

        $('#zero_config').DataTable();

        $(document).ready(function(){
            // For quantity validation
     $(".numeric_feild").on("focus",function(event)
     {
        id=$(this).attr('id');
        var text = document.getElementById(id);
        text.onkeypress = text.onpaste = checkInput;
     });
    function checkInput(e)
    {
    var e = e || event;
    var char = e.type == 'keypress'
    ? String.fromCharCode(e.keyCode || e.which)
    : (e.clipboardData || window.clipboardData).getData('Text');
    if (/[^\d]/gi.test(char)) {
    return false;
    }
    }

    //For discount validation
    $(".numeric_feild_discount").keypress(function(event){
        return isNumber(event, this);
    });

     function isNumber(evt, element)
     {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (
                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
                return false;

                return true;
      }
});
function save_admin_message_settings(id)
    {
        var status = $(".status_"+id+":checked").val();
        if(status!=1)
            status =0;
        var type = $(".status_"+id).attr('type_status');
        //alert(type);
        var _token = $('input[name="csrf-token"]').val();
             $.ajaxSetup({

          headers: {

              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

          }

      });

        $.ajax({
            url:"{{url('admin/update-status')}}",
            type:"post",
            data:{id:id,status:status,_token:_token,type:type},
            success:function(data){
                    var msg= "Status Updated Successfully";
                    var title= "Success";
                    var type= "success";
                    notification_msg(msg,title,type);
            }
        });
    }
    function notification_msg(msg,title,type)
    {
              var shortCutFunction = type;//'success'; //$("#toastTypeGroup input:radio:checked").val();
                //alert(shortCutFunction);
                var msg = msg;
                var title = title;
                var $showDuration = 300;
                var $hideDuration = 1000;
                var $timeOut = 5000;
                var $extendedTimeOut = 1000;
                //var toastIndex = toastCount++;

                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if(typeof $toast === 'undefined'){
                    return;
                }

    }
    function delete_entry(form) {
        // self.preventDefault();

        // alert(self);
        bootbox.confirm({
            message: "Are you sure you want to delete this entry?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result) {
                    $("#"+form).submit();
                }
                console.log('This was logged in the callback: ' + result);
            }
        });
    }

    </script>
    @stack('scripts')
</body>

</html>
