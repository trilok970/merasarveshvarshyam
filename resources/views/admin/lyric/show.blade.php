@extends('layouts.admin')
@section('title','Lyric Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Lyric</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/lyric')}}">Lyric</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">Lyric Details</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/lyric')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Lyrics" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Lyrics
                                        </a>
                                    </div>
                                </div>

                                <div class="table-responsive">


                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <td><b>{{$lyric->title}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Category</th>
                                                <td>
                                                @foreach($lyric->lyricCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}"><b>{{$value->music_category->name}}</b></span>
                                                @endforeach

                                                </td>

                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td><b>{{$lyric->description}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Image</th>
                                                <td><a target="_blank" href="{{$lyric->image}}"><img src="{{$lyric->image}}" width="50" /></a></td>
                                            </tr>
                                            <tr>
                                                <th>File</th>
                                                <td><a download target="_blank" href="{{$lyric->file}}"><i class="far fa-file-word"></i></a></td>
                                            </tr>

                                        </thead>



                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
