@extends('layouts.admin')
@section('title','Cms Pages')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">CMS Pages</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/cms')}}">CMS Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">
                               
                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"> <h5 class="card-title">All Pages</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/cms/create')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All CMS Pages" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            Add Page
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Title</th>
                                                <th>Slug</th>
                                                <th>Update</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($datas as $row)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$row->title}}</td>
                                                <td>{{$row->slug}}</td>
                                                <td>{{date('d-M-Y',strtotime($row->updated_at))}}</td>
                                                <td><a href="{{url(SEGMENT.'/cms/'.$row->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fa far fa-edit"></i></a></td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach
                                        
                                    </table>
                                </div>

                            </div>
                        </div>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
          
@endsection