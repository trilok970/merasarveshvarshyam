@extends('layouts.admin')
@section('title','Product Edit')
@section('content')
@push('stylesheets')
<style>
    .image-checkbox
    {
        cursor: pointer;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        border: 4px solid transparent;
        outline: 0;
    }

        .image-checkbox input[type="checkbox"]
        {
            display: none;
        }

    .image-checkbox-checked
    {
        border-color: #f58723;
    }
</style>

@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Product</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/product')}}">Product</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">

                    <div class="alert d-none" role="alert" id="error-msg"></div>
                    <form class="form-horizontal  add-form" enctype="multipart/form-data"  data-url="{{route('product.index')}}" action="{{url(SEGMENT.'/product/'.$product->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method("PUT")
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/product')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Product" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Product
                                        </a>
                                    </div>

                            </div>

                                <div class="form-group row">

                                </div>
                               <div class="form-group row">

                                    <label class="col-md-2 m-t-15">Category</label>
                                    <div class="col-md-4">
                                        <select type="text" class="form-control" id="category_id" name="category_id" placeholder="" onchange="show_subcategory()">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $cat)
                                        <option value="{{$cat->id}}" {{ $product->category_id==$cat->id ? "selected":"" }}>{{$cat->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('category_id')}}</span>
                                    </div>
                                    <label class="col-md-2 m-t-15">Subcategory</label>
                                    <div class="col-md-4">
                                        <select type="text" class="form-control" id="subcategory_id" name="subcategory_id" placeholder="" >
                                        <option value="">Select subategory</option>
                                        @foreach($subcategories as $sub)
                                        <option value="{{$sub->id}}" {{ $product->subcategory_id==$sub->id ? "selected":"" }}>{{$sub->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('subcategory_id')}}</span>
                                    </div>
                                </div>
                               <div class="form-group row">

                                    <label class="col-md-2 m-t-15">Product Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Product Name" value="{{$product->name}}">

                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                    <label class="col-md-2 m-t-15">Product Hindi Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="hindi_name" name="hindi_name" placeholder="Product Hindi Name" value="{{$product->hindi_name}}">

                                        <span class="text-danger">{{$errors->first('hindi_name')}}</span>
                                    </div>

                                </div>
                                <div class="form-group row">

                                    {{-- <label class="col-md-2 m-t-15">Product Qty</label>
                                    <div class="col-md-4">
                                        <input type="number" min="0" class="form-control" id="qty" name="qty" placeholder="Product Qty" value="{{$product->qty}}">

                                        <span class="text-danger">{{$errors->first('qty')}}</span>
                                    </div> --}}
                                    <label class="col-md-2 m-t-15">Product Code</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Product Code" value="{{$product->code}}">

                                        <span class="text-danger">{{$errors->first('code')}}</span>
                                    </div>
                                </div>
                                {{-- <div class="form-group row">

                                    <label class="col-md-2 m-t-15">Product Min Qty</label>
                                    <div class="col-md-4">
                                        <input type="number" min="0" class="form-control" id="min_qty" name="min_qty" placeholder="Product Min Qty" value="{{$product->min_qty}}">

                                        <span class="text-danger">{{$errors->first('min_qty')}}</span>
                                    </div>
                                    <label class="col-md-2 m-t-15">Product Max Qty</label>
                                    <div class="col-md-4">
                                        <input type="number" min="0" class="form-control" id="max_qty" name="max_qty" placeholder="Product Max Qty" value="{{$product->max_qty}}">

                                        <span class="text-danger">{{$errors->first('max_qty')}}</span>
                                    </div>
                                </div> --}}
                                {{-- <div class="form-group row">

                                    <label class="col-md-2 m-t-15">Product Price</label>
                                    <div class="col-md-4">
                                        <input type="number" min="0" class="form-control" id="price" name="price" placeholder="Product Price" value="{{$product->price}}">

                                        <span class="text-danger">{{$errors->first('price')}}</span>
                                    </div>
                                    <label class="col-md-2 m-t-15">Product Mrp</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" id="mrp" name="mrp" placeholder="Product Mrp" step="1" min="0" pattern="^\d*(\.\d{0,2})?$" value="{{$product->mrp}}">

                                        <span class="text-danger">{{$errors->first('mrp')}}</span>
                                    </div>
                                </div> --}}
                                <div class="form-group row">

                                    <label class="col-md-2 m-t-15">Product Unit</label>
                                    <div class="col-md-4">
                                        <select type="text" class="form-control" id="unit" name="unit" placeholder="" onchange="show_subunit()" >
                                        <option value="">Select unit</option>
                                        @foreach($units as $unit)
                                        <option value="{{$unit->id}}" {{ $product->unit==$unit->id ? "selected":"" }} >{{$unit->name}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('unit')}}</span>
                                    </div>
                                    <label class="col-md-2 m-t-15">Product Weight</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="weight" name="weight" placeholder="Product Weight" value="{{$product->weight}}" />

                                        <span class="text-danger">{{$errors->first('weight')}}</span>
                                    </div>
                                </div>
                                <div class="subunit">
                                    
                                    @if(count($product->ProductSubunit) > 0)

                                    <div class="form-group row">
                                        <label class="col-md-2 m-t-15">Unit</label>
                                        <label class="col-md-2 m-t-15">Price</label>
                                        <label class="col-md-2 m-t-15">Mrp</label>
                                        <label class="col-md-2 m-t-15">Qty</label>
                                        <label class="col-md-1 m-t-15">Min Qty</label>
                                        <label class="col-md-1 m-t-15">Max qty</label>
                                        <label class="col-md-2 m-t-15">Colors</label>
                                    </div>
                                    @php
                                        // echo "<pre>";
                                        // print_r($product->ProductSubunit);exit;
                                    @endphp
                                    @foreach ($product->ProductSubunit as $product_subunit)
                                    <div class="form-group row subunit_date_div">
                                        <label class="col-md-2 m-t-15">
                                            <select class="subunit_id form-control" name="subunit_id[]">';
                                               <option value="">Select Subunit</option>
                                    
                                                @foreach($subunits as $subunit)
                                                <option value="{{$subunit->id}}" {{($product_subunit->subunit_id == $subunit->id)? "selected":""}}>{{$subunit->name}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                        <label class="col-md-2 m-t-15"><input type="number" min="0" class="form-control price" id="price" name="price[]" placeholder="Product Price" value="{{$product_subunit->price}}">
                                        </label>
                                        <label class="col-md-2 m-t-15"><input type="number" class="form-control mrp" id="mrp" name="mrp[]" placeholder="Product Mrp" step="1" min="0" pattern="^\d*(\.\d{0,2})?$" value="{{$product_subunit->mrp}}">
                                        </label>
                                        <label class="col-md-2 m-t-15"><input type="number" min="0" class="form-control qty" id="qty" name="qty[]" placeholder="Product Qty" value="{{$product_subunit->qty}}">
                                        </label>
                                        <label class="col-md-1 m-t-15"><input type="number" min="0" class="form-control min_qty" id="min_qty" name="min_qty[]" placeholder="Product Min Qty" value="{{$product_subunit->min_qty}}">
                                        </label>
                                        <label class="col-md-1 m-t-15"><input type="number" min="0" class="form-control max_qty" id="max_qty" name="max_qty[]" placeholder="Product Max Qty" value="{{$product_subunit->max_qty}}">
                                        </label>
                                        <label class="col-md-2 m-t-15">
                                            <select class="color_id form-control" name="color_id[]">
                                                <option value="">Select Color</option>
                                                 @foreach($colors as $color)
                                                 <option value="{{$color->id}}" {{($product_subunit->color_id == $color->id)? "selected":""}} style="background: {{$color->code}};">{{$color->code}}</option>
                                                 @endforeach
                                             </select>
                                        </label>
                                    </div>
                                    @endforeach
                                    
                                    @endif
                                </div>
                                <div class="form-group row subunit_add_remove_div" >
                                    <label class="col-md-12 m-t-15">
                                        <button class="btn btn-sm btn-success add-subunit"><i class="fas fa-plus-square"></i>Add</button>&nbsp;
                                        <button class="btn btn-sm btn-danger remove-subunit"><i class="fas fa-minus-square"></i>Remove</button>
                                    </label>
                                </div>
                                <div class="form-group row">


                                    <label class="col-md-2 m-t-15">Product Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="description" name="description" placeholder="Product description" >{{$product->description}}</textarea>

                                        <span class="text-danger">{{$errors->first('description')}}</span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Images</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="image" name="image[]" placeholder="Choose image" value="{{old('image')}}" multiple="">

                                    </div>
                                    <div class="col-lg-4">
                                    <img style="display:none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
                                        <span class="text-danger">{{$errors->first('image')}}</span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Old Images</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            @foreach ($product->ProductImages as $image)
                                            <div class="col-md-2 text-center">
                                                <label class="image-checkbox" title="England">
                                                    <img src="{{ $image->image }}" width="100" />
                                                    <input type="checkbox" name="product_image[]" value="{{ $image->id }}"  />
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                        <span class="text-danger">Please select image for delete</span>

                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Thumbnail</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control" id="thumbnail" name="thumbnail" placeholder="Choose image" >

                                    </div>
                                    <div class="col-lg-4">
                                    <img  id="thumbnail_preview"  src="{{ $product->thumbnail}}" width="70" class="pull-right" alt="User Image">
                                        <span class="text-danger">{{$errors->first('thumbnail')}}</span>

                                    </div>
                                </div>



                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/product')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            @push('scripts')
     <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
     <script src="{{asset('dist/js/product/event.js')}}"></script>
          <script>

    function show_subcategory()
    {
      var category_id = $('#category_id').children('option:selected').attr('value');

      $.ajax({
          type:'get',
          url:"{{url('admin/ajax')}}",
          data:{category_id:category_id,"type":"subcategory"},
           success:function(data){

                  $('#subcategory_id').html(data);
                }
              });

    }
    function show_subunit()
    {
      var unit_id = $('#unit').children('option:selected').attr('value');

      $.ajax({
          type:'get',
          url:"{{url('admin/ajax')}}",
          data:{unit_id:unit_id,"type":"subunit"},
           success:function(data){
               if(data) {
                  $('.subunit').html(data);
               }
                }
              });

    }
        $(document).ready(function(){

           CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 300;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('description');
            $(".add-subunit").click(function(e) {
                e.preventDefault();
                var clone = $(".subunit_date_div").clone(true);
                $(".subunit").append(clone);     
                         

            });
            $(".remove-subunit").click(function(e) {
                e.preventDefault();
                $(".subunit_date_div").last().remove();           
            });





     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#thumbnail_preview').attr('src', e.target.result);

        $('#thumbnail_preview').hide();
        $('#thumbnail_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#thumbnail").change(function() {
    readURL(this);
    });



        });






    </script>
    <script type="text/javascript">
        jQuery(function ($) {
            // init the state from the input
            $(".image-checkbox").each(function () {
                if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                    $(this).addClass('image-checkbox-checked');
                }
                else {
                    $(this).removeClass('image-checkbox-checked');
                }
            });

            // sync the state to the input
            $(".image-checkbox").on("click", function (e) {
                if ($(this).hasClass('image-checkbox-checked')) {
                    $(this).removeClass('image-checkbox-checked');
                    $(this).find('input[type="checkbox"]').first().removeAttr("checked");
                }
                else {
                    $(this).addClass('image-checkbox-checked');
                    $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
                }

                e.preventDefault();
            });
        });
    </script>
    @endpush
@endsection
