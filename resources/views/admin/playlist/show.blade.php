@extends('layouts.admin')
@section('title','Playlist Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Playlist</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/playlist')}}">Playlist</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">
                               
                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">All Playlists</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/playlist')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Playlists" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Playlists
                                        </a>
                                    </div>
                                </div>
                               
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Channel Name</th>
                                                <th>Category Name</th>
                                                <th>Image</th>
                                                <th>Updated</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            
                                            <tr>
                                                <td>{{$playlist->name}}</td>
                                                <td>{{$playlist->channel->name}}</td>
                                                <td>{{$playlist->category->name}}</td>
                                                <td><a target="_blank" href="{{url('/images/'.$playlist->image)}}"><img src="{{url('/images/'.$playlist->image)}}" width="50" /></a></td>
                                                <td>{{date('d-M-Y',strtotime($playlist->updated_at))}}</td>
                                                
                                               
                                            </tr>
                                            
                                            
                                        
                                    </table>
                                     <div class="form-group row">
                                    <label class="col-md-12 m-t-15"><h5 class="card-title">Songs</h5></label>
                                   
                                   </div>
                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Image</th>
                                                <th>File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($playlist->songs as $song)
                                            <tr>
                                                <td>{{$song->title}}</td>
                                                <td><a target="_blank" href="{{url('uploads/images/'.$song->image)}}"><img src="{{url('uploads/images/'.$song->image)}}" width="50" /></a></td>
                                                <td><audio controls>
                                                <source src="{{ url('uploads/audio/'.$song->file) }}" type="audio/{{$song->extension}}">
                                                Your browser does not support the audio element.
                                                </audio></td>
                                            </tr>
                                            @endforeach
                                            
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
          
@endsection