@extends('layouts.admin')
@section('title','Orders')
@section('content')
<style>
.cus_box_info
{
    background-color: #24313C; padding: 10px; margin: 10px;
}

.cus_body_box
{
    background-color: #1C2331; padding: 10px; margin: 10px;color:white;
}

.cus_head_box
{
    height: 40px; padding-top:10px; margin-top: 0px; background-color:#1AB188;
}

.detail_cus  { padding-top: 7px; margin-top: 7px; }
.box_footer_cus  { background-color: #1AB188; padding-top: 7px; margin-top: 7px; }


.status_checkbox{
visibility: hidden;
}

/* SLIDE THREE */
.slideparam {
width: 80px;
height: 26px;
background: #333;
margin: 2px auto;

-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;
position: relative;

-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);

}

.slideparam:after {
content: 'OFF';
font: 12px/26px Arial, sans-serif;
color: #7a121d;
position: absolute;
right: 10px;
z-index: 0;
font-weight: bold;
text-shadow: 1px 1px 0px rgba(255,255,255,.15);
}

.slideparam:before {
content: 'ON';
font: 12px/26px Arial, sans-serif;
color: #00bf00;
position: absolute;
left: 10px;
z-index: 0;
font-weight: bold;
}

.slideparam label {
display: block;
width: 34px;
height: 20px;
cursor: pointer;
-webkit-border-radius: 50px;
-moz-border-radius: 50px;
border-radius: 50px;

-webkit-transition: all .4s ease;
-moz-transition: all .4s ease;
-o-transition: all .4s ease;
-ms-transition: all .4s ease;
transition: all .4s ease;
position: absolute;
top: 3px;
left: 3px;
z-index: 1;

-webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
-moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
background: #fff;


filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 );
}

.slideparam input[type=checkbox]:checked + label {
left: 43px;
}


 .slideparam input[type=checkbox]:checked + label:after {
   background: #27ae60;
}

.slideparam label:after {
   content:'';
   width: 10px;
   height: 10px;
   position: absolute;
   top: 5px;
   left: 12px;
   background: red;
   border-radius: 50%;
   box-shadow: inset 0px 1px 1px black, 0px 1px 0px rgba(255, 255, 255, 0.9);
 }

</style>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Orders</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/order')}}">Orders</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"> <h5 class="card-title">All Orders</h5></label>
                                    <div class="col-md-10">
                                         {{-- <a href="{{url(SEGMENT.'/order/create')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="Add Promo Code" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            Add Promo Code
                                        </a> --}}
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Order No.</th>
                                                <th>Price</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($orders as $order)
@if($order->order_status=="delivered")

<?php $order_stats_txt = '<a  data-toggle="tooltip" title=""  data-original-title="Delivery Details" style="cursor:pointer;" text="Delivery Details" order_id="'.$order->id.'" table_name="order_deliveries" class="order_details delivery" ><span  class="text-success " ><i class="fa fa-check" aria-hidden="true"></i> Delivered</span></a>';
 ?>
@elseif($order->order_status=="pending")

<?php $order_stats_txt = "<span class='text-warning'><i class='fa fa-clock-o' aria-hidden='true'></i> Pending</span>"; ?>
@elseif($order->order_status=="received")

<?php $order_stats_txt = '<span class="text-info"><i class="fa fa-clock-o" aria-hidden="true"></i> Received</span>'; ?>
@elseif($order->order_status=="shipped")

<?php $order_stats_txt = '<a data-toggle="tooltip" title="" data-original-title="Shipping Details" style="cursor:pointer;" text="Shipping Details" order_id="'.$order->id.'" table_name="order_shippings" class="order_details shipping" ><span class="text-primary" url="{{url(order-details/$order->id/order_deliveries)}}"><i class="fa fa-clock-o" aria-hidden="true"></i> Shipped</span></a>'; ?>

@elseif($order->order_status=="canceled")

<?php $order_stats_txt = '<a data-toggle="tooltip" title=""  data-original-title="Cancel Details" style="cursor:pointer;" text="Cancel Details" order_id="'.$order->id.'" table_name="order_cancels" class="order_details cancel" ><span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i> Canceled</span></a>'; ?>


@endif
@if($order->status=="failed")

<?php $order_stats_txt = '<a data-toggle="tooltip" title=""  data-original-title="
Failed Details" style="cursor:pointer;" class="order_details" ><span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i> Failed</span></a>'; ?>

@endif
@include(SEGMENT.'.order.model_orders')
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$order->order_no}}</td>
                                                <td>{{$order->price}}</td>
                                                <td>{{$order->user->fullname}}</td>
                                                <td>{{date('d-M-Y',strtotime($order->order_date))}}</td>
                                                <td><?php echo $order_stats_txt;?></td>
                                                <td>
                                                    <input type="hidden" name="token" id="token" value="{{$order->token}}" />
                                                    <input type="hidden" name="id" id="id" value="{{$order->id}}" />
                                                    <button  type="button" id="{{$order->id}}" url="{{url(SEGMENT.'/order/'.$order->id)}}" class="btn btn-sm btn-warning view1" ><i class="fa fa-eye" aria-hidden="true"></i></button>

                                                    @if($order->order_status=="pending")
                                                      <a class="pending" href="{{url(SEGMENT.'/update-order-status/'.$order->id.'/'.$order->user_id.'/received')}}" >
                                                    <button class="btn btn-success btn-sm " style="padding-top: 3px;">Accept</button>
                                                    </a><span>
                                                    <button data-toggle="modal" data-target="#declined_model{{$order->id}}"  class="btn btn-danger decline_custom_btn btn-sm"  style="padding-top: 3px;">Decline</button>
                                                                                </span>
                                                    @elseif($order->order_status=="received")
                                                    <button style="width: 137px;" data-toggle="modal" data-target="#received_model{{$order->id}}"  class="btn btn-primary  btn-sm"  style="padding-top: 3px;">Confirm Shipping</button>
                                                    @elseif($order->order_status=="shipped")
                                                    <button style="width: 137px;" class="btn btn-success  btn-sm" data-toggle="modal" data-target="#shipped_model{{$order->id}}"  style="padding-top: 3px;">Confirm Delivery</button>
                                                    @elseif($order->order_status=="delivered")
                                                    <span class="text-success">Delivered</span>
                                                    @elseif($order->order_status=="canceled")
                                                    <span class="text-danger">Canceled</span>
                                                    @elseif($order->order_status=="failed")
                                                    <span class="text-danger">Failed</span>
                                                    @endif

                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach

                                    </table>
                                </div>


                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>

            <!-------------------Modal Start----------------->
			  <div class="container">

                <!-- Trigger the modal with a button -->

<!-- Modal -->
<div class="modal fade" id="view_modal" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Order Details</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body view_body">

</div>
<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>

</div>
                  </div>
                        </div>
</div>
<!-------------------Modal End----------------->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
          @push('scripts')
            <script >
                $(function () {
              $('#example1').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true,
                'pageLength'  : 50,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                  { "bSortable": false },
                                  { "bSortable": false }
                              ]

              });


            });
            </script>
            <script>
                $(document).ready(function(){
                    $(".shipping").click(function(e){
                        e.preventDefault();
                        var target = $(this).attr('id');

                        $(target).modal();
                    });
                    $(".cancel").click(function(e){
                        e.preventDefault();
                        var target = $(this).attr('id');

                        $(target).modal();
                    });
                    $(".delivery").click(function(e){
                        // e.preventDefault();
                        var target = $(this).attr('id');
                        $(target).modal();
                    });

                //For view order Details
            $(".view1").click(function(e){
                e.preventDefault();
                $("#view_modal").modal();
                $(".view_body").html('');
              var order_id = $(this).attr('id');

              var url =  $(this).attr('url');
              $.ajax({
                  url:url,
                  data:{order_id:order_id},
                  type:"get",
                  success:function(data)
                  {
                      $(".view_body").html(data);
                  }
              })
          });

          //For Accept Order
          $(".pending").click(function(e){
            e.preventDefault();
            var id = $(this).attr('id');
             bootbox.confirm({
                  message:"Are you sure you want confirm this order?",
                  buttons:{ cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    },
                      },
                    callback: function (result) {
                        if(result){
                        var href = $(".pending").attr('href');
                        location.href=href;

                        }
                    }
                  })//confirm
         });
                });

            </script>
         @endpush

@endsection
