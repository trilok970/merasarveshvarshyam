@extends('layouts.admin')
@section('title','Promo Code Edit')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Promo Code</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/promocode')}}">Promo Code</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    
                   <div class="col-md-12">
                       
                    
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url(SEGMENT.'/promocode/'.$promocode->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method("PUT")
                   <div class="card">
                    
                    
                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/promocode')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Promo Code" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Promo Code
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Code</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Enter Code" value="{{$promocode->code}}">
                                        <span class="text-danger">{{$errors->first('code')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Type</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="type" name="type" >
                                            <option value="">Select type</option>
                                            <option value="flat" {{$promocode->type=='flat' ? 'selected':''}}>Flat</option>
                                            <option value="percentage" {{$promocode->type=='percentage' ? 'selected':''}}>Percentage</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('type')}}</span>
                                    </div>
                                </div>
                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Discount</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="discount" name="discount" placeholder="Enter Discount" value="{{$promocode->discount}}">
                                        <span class="text-danger">{{$errors->first('discount')}}</span>
                                    </div>
                                </div>
                                
                                
                                
                                
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/promocode')}}" class="btn btn-danger resetBtn">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
           
          <script>
        $(document).ready(function(){
             
            
            /* validate */

        

        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                title: {
                    required: true
                },
                sub_title: {
                    required: true
                },
                description: {
                    required: true
                },
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });
         
    
     function readURL(input) 
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });
        
        
        
        });

        

        
        
        
    </script>
@endsection