@extends('layouts.admin')
@section('title','Audio Show')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Audio</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/audio')}}">Audio</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                   <div class="col-md-12">
                   @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
                   <div class="card">
                            <div class="card-body">

                                 <div class="form-group row">
                                    <label class="col-md-2 m-t-15"><h5 class="card-title">Audio Details</h5></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/audio')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Audios" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Audios
                                        </a>
                                    </div>
                                </div>

                                <div class="table-responsive">


                                     <table id="example1" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <td><b>{{$audio->title}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Category</th>
                                                <td>
                                                @foreach($audio->AudioCategory as $value)
                                                      <span class="badge badge-{{$colors[$loop->index]}}"><b>{{$value->music_category->name}}</b></span>
                                                @endforeach

                                                </td>

                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td><b>{{$audio->description}}</b></td>
                                            </tr>
                                            <tr>
                                                <th>Image</th>
                                                <td><a target="_blank" href="{{$audio->image}}"><img src="{{$audio->image}}" width="50" /></a></td>
                                            </tr>
                                            <tr>
                                                <th>File</th>
                                                <td>
                                                <audio controls>
                                                <source src="{{ $audio->file }}" type="audio/{{$audio->extension}}">
                                                Your browser does not support the audio element.
                                                </audio></td>
                                            </tr>

                                        </thead>



                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection
