@extends('layouts.admin')
@section('title','Unit Create')
@section('content')
@push('stylesheets')
@endpush
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Unit</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/unit')}}">Unit</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    
                   <div class="col-md-12">
                    <div class="alert d-none" role="alert" id="error-msg"></div>
                       
                    
                    <form class="form-horizontal add-form" enctype="multipart/form-data" data-url="{{route('color.index')}}" action="{{url(SEGMENT.'/color')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">
                    
                    
                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/unit')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Unit" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Unit
                                        </a>
                                    </div>
                            </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Name</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control name" id="name" name="name" placeholder="Enter name" value="">
                                        <div id="hsvflat"></div>
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/unit')}}" class="btn btn-danger resetBtn">Cancel</a>
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
           
           @push('scripts')
            <script src="{{asset('dist/js/common/event.js')}}"></script>
    @endpush
@endsection