@extends('layouts.admin')
@section('title','Delivery Charge Create')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Delivery Charge</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url(SEGMENT.'/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url(SEGMENT.'/deliverycharge')}}">Delivery Charge</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">

                   <div class="col-md-12">


                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url(SEGMENT.'/deliverycharge')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">


                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url(SEGMENT.'/deliverycharge')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Delivery Charge" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Delivery Charge
                                        </a>
                                    </div>
                            </div>


                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Value</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="value" name="value" placeholder="Enter value" value="{{old('value')}}">
                                        <span class="text-danger">{{$errors->first('value')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Amount</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="amount" name="amount" placeholder="Enter Amount" value="{{old('amount')}}">
                                        <span class="text-danger">{{$errors->first('amount')}}</span>
                                    </div>
                                </div>






                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url(SEGMENT.'/deliverycharge')}}" class="btn btn-danger resetBtn">Cancel</a>

                                </div>
                            </div>
                        </div>
                    </form>
                        </div>


                </div>
                <!-- ============================================================== -->


                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

          <script>
        $(document).ready(function(){

            /* validate */



        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                category_id: {
                    required: true
                },
                name: {
                    required: true
                },
                image: {
                    required: true
                },

            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });


     function readURL(input)
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $('#image_preview').attr('src', e.target.result);

        $('#image_preview').hide();
        $('#image_preview').fadeIn(650);
        }
       reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
    readURL(this);
    });



        });






    </script>
@endsection
