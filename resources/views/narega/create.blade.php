<!DOCTYPE html>
<html lang="en">
<head>
	<title>Contact V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{url('assets/narega/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/css/main.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
<!--===============================================================================================-->
</head>
<body>

	<div class="bg-contact2" style="background-image: url( {{ url('assets/narega/images/bg-01.jpg') }});">
		<div class="container-contact2">
			<div class="wrap-contact2">
				<form class="contact2-form" method="post" action="{{ url('narega') }}">
                    @csrf
					<span class="contact2-form-title">
						Contact Us
					</span>

					<div class="wrap-input2 validate-input" data-validate="Name is required">
						<input class="input2" type="text" name="name">
                        <span class="text-danger" >{{ $errors->first('name') }}</span>
						<span class="focus-input2" data-placeholder="NAME (*)"></span>
					</div>
                    <div class="wrap-input2 validate-input" data-validate="Father Name is required">
						<input class="input2" type="text" name="father_name">
                        <span class="text-danger" >{{ $errors->first('father_name') }}</span>
						<span class="focus-input2" data-placeholder="Father Name (*)"></span>
					</div>

					<div class="wrap-input2 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input2" type="text" name="email">
						<span class="focus-input2" data-placeholder="EMAIL"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="Mobile No. is required">
						<input class="input2" type="text" name="mobile_no">
                        <span class="text-danger" >{{ $errors->first('mobile_no') }}</span>
						<span class="focus-input2" data-placeholder="Mobile No. (*)"></span>
					</div>
                    <div class="wrap-input2 validate-input" data-validate="Date of birth is required">
						<input class="input2 datepicker" type="text" name="dob">
                        <span class="text-danger" >{{ $errors->first('dob') }}</span>
						<span class="focus-input2" data-placeholder="Date Of Birth (*)"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="Post is required">
						<input class="input2" type="text" name="post" >
                        <span class="text-danger" >{{ $errors->first('post') }}</span>
						<span class="focus-input2" data-placeholder="Post(Designation) (*)"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="Date of joining is required">
						<input class="input2 datepicker" type="text" name="date_of_joining">
                        <span class="text-danger" >{{ $errors->first('date_of_joining') }}</span>
						<span class="focus-input2" data-placeholder="Date Of Joining (*)"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="Gram panchayat is required">
						<input class="input2" type="text" name="gram_panchayat">
						<span class="focus-input2" data-placeholder="Gram Panchayat"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="Block is required">
						<input class="input2" type="text" name="block">
                        <span class="text-danger" >{{ $errors->first('block') }}</span>
						<span class="focus-input2" data-placeholder="Block (*)"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="District is required">
						<input class="input2" type="text" name="district">
                        <span class="text-danger" >{{ $errors->first('district') }}</span>
						<span class="focus-input2" data-placeholder="District (*)"></span>
					</div>
					<div class="wrap-input2 validate-input" data-validate="State is required">
						<input class="input2" type="text" name="state">
                        <span class="text-danger" >{{ $errors->first('state') }}</span>
						<span class="focus-input2" data-placeholder="State (*)"></span>
					</div>
					{{-- <div class="wrap-input2 validate-input" data-validate="Donation is required">
						<input class="input2" type="text" name="donation">
                        <span class="text-danger" >{{ $errors->first('donation') }}</span>
						<span class="focus-input2" data-placeholder="Donation (*)"></span>
					</div> --}}

					<div class="wrap-input2 validate-input" data-validate = "Message is required">
						<textarea class="input2" name="message"></textarea>
						<span class="focus-input2" data-placeholder="MESSAGE"></span>
					</div>

					<div class="container-contact2-form-btn">
						<div class="wrap-contact2-form-btn">
							<div class="contact2-form-bgbtn"></div>
							<button class="contact2-form-btn">
								Send Your Message
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{url('assets/narega/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/js/main.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');

      $(document).ready(function(){
        $('.datepicker').datepicker();
       });
	</script>

</body>
</html>
