<!DOCTYPE html>
<html lang="en">
<head>
	<title>Contact V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{url('assets/narega/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/css/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/datatables.net-bs4/css/jquery.dataTables.min.css')}}">

<!--===============================================================================================-->
</head>
<body>

	<div class="bg-contact2" style="background-image: url( {{ url('assets/narega/images/bg-01.jpg') }});">
		<div class="container-contact2">
			<div class="wrap-contact2 col-md-12">
                <a class="btn btn-success pull-right" href="{{ url('narega-export') }}" >Export List</a>
                <div class="table-responsive">
                    <table id="example1" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                <th>Mobile</th>
                                <th>DOB</th>
                                <th>Post</th>
                                <th>DOJ</th>
                                <th>Gram Panchayat</th>
                                <th>Block</th>
                                <th>District</th>
                                <th>State</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach($datas as $row)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->father_name}}</td>
                                <td>{{$row->mobile_no}}</td>
                                <td>{{date('d-M-Y',strtotime($row->dob))}}</td>
                                <td>{{$row->post}}</td>
                                <td>{{date('d-M-Y',strtotime($row->date_of_joining))}}</td>
                                <td>{{$row->gram_panchayat}}</td>
                                <td>{{$row->block}}</td>
                                <td>{{$row->district}}</td>
                                <td>{{$row->state}}</td>
                            </tr>
                            @php $i++; @endphp
                            @endforeach
                        </tbody>

                    </table>
                </div>
			</div>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{url('assets/narega/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/js/main.js')}}"></script>
	<script src="{{url('assets/narega/vendor/datatables.net-bs4/js/jquery.dataTables.min.js')}}"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script >
        $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true,
        'pageLength'  : 50,
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "aoColumns": [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                      ]

      });


    });
    </script>

</body>
</html>
