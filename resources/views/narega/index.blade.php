<!DOCTYPE html>
<html lang="en">
<head>
	<title>Contact V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{url('assets/narega/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('assets/narega/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>

	<div class="bg-contact2" style="background-image: url( {{ url('assets/narega/images/bg-01.jpg') }});">
		<div class="container-contact2">
			<div class="wrap-contact2">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            You can submit more info ........ <a href="{{ url('narega/create')}}"> <b>Narega Form</b></a>
			</div>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{url('assets/narega/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{url('assets/narega/js/main.js')}}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>


      $(document).ready(function(){
        $('.datepicker').datepicker();
       });
	</script>

</body>
</html>
